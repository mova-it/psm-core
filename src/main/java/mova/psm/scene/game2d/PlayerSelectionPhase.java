package mova.psm.scene.game2d;

import mova.Settings;
import mova.game.dice.Die;
import mova.game.dice.Rollable;
import mova.psm.ecs.PSMGameObject;
import mova.util.CollectionUtils;
import mova.util.function.FunctionalInterfaceUtils;

import java.util.Collections;

class PlayerSelectionPhase extends GameModePhase<ConfigurationMode> {

    private PSMGameObject selectPlayerObject;
    private long playerSelectionTime;
    private int step;

    protected PlayerSelectionPhase(ConfigurationMode mode) {
        super(mode);
    }

    @Override
    void entry() {
        SplashScreen splashScreen = new SplashScreen();
        splashScreen.setSize(mode.gameWindow.getSize());
        splashScreen.setFont(Settings.DIALOG_FONT.deriveFont(50f));

        selectPlayerObject = new PSMGameObject("SELECT_PLAYER");
        selectPlayerObject.addComponent(splashScreen);
        mode.scene.addGameObject(selectPlayerObject);
        selectPlayerObject.show();

        step = 0;
        playerSelectionTime = 0; // Le temps doit être initialisé quand le mode a fini d'être initialisé
    }

    private final Runnable chosePlayer = FunctionalInterfaceUtils.onlyOnce(() -> {
        // TODO comme le joueur n'a pas encore été sélectionné on affiche un amiral par défaut ce qui n'est pas bon. On ne devrait rien affiché tant qu'il n'y a rien de choisi
        selectPlayerObject.getComponent(SplashScreen.class).setText("Selection du joueur");
        Rollable rollable = Die.getRollable(mode.scene.admirals.size());
        int result = rollable.roll();
        CollectionUtils.move(mode.scene.admirals, result - 1, 0);
        if (mode.scene.admirals.size() > 2) Collections.shuffle(mode.scene.admirals.subList(1, mode.scene.admirals.size()));
        mode.scene.setAdmiral(0);
    });

    private final Runnable showPlayer = FunctionalInterfaceUtils.onlyOnce(() -> selectPlayerObject.getComponent(SplashScreen.class).setText(mode.scene.getCurrentAdmiral().getName() + " sera le premier joueur."));

    @Override
    void update(long dt) {
        switch (step) {
            case 0:
                chosePlayer.run();
                break;
            case 1:
                showPlayer.run();
                break;
            case 2:
                mode.nextPhase();
                break;
            default:
                throw new IllegalStateException("Etape illégale: " + step);
        }

        if (playerSelectionTime > 1000) {
            step++;
            playerSelectionTime -= 1000;
        }

        playerSelectionTime += dt;
    }

    @Override
    void release() {
        selectPlayerObject.destroy();
    }
}
