package mova.psm.scene.game2d;

import mova.game.core.ecs.AbstractGameComponent;
import mova.game.core.ecs.GameObject;
import mova.game.graphics.MovaGraphics;
import mova.game.graphics.Transform;
import mova.game.model.DescriptionProvider;

import java.awt.*;
import java.util.stream.Collectors;

class DescriptionOverlay extends AbstractGameComponent {

    private final Rectangle bounds;

    private String description;

    public DescriptionOverlay(Rectangle bounds) {
        this.bounds = bounds;
    }

    @Override
    public void update(long dt) {
        description = getCurrentDescription();
    }

    private String getCurrentDescription() {
        GameObject selectedGameObject = ((GameScene) getGameObject().getScene()).getSelectedGameObject();
        if (selectedGameObject != null) {
            DescriptionProvider descriptionProvider = selectedGameObject.getComponent(DescriptionProvider.class);
            if (descriptionProvider != null) {
                return descriptionProvider.getDescriptions().entrySet().stream().map(entry -> entry.getKey() + ": " + entry.getValue()).collect(Collectors.joining("\n"));
            }
        }

        return null;
    }

    @Override
    public void draw(MovaGraphics movaGraphics) {
        if (description != null) {
            Rectangle transformedBounds = getGameObject().getComponent(Transform.class).apply(bounds).getBounds();
            movaGraphics.setColor(Color.WHITE);
            movaGraphics.drawStringWithinBounds(description, transformedBounds);
        }
    }
}
