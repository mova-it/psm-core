package mova.psm.asset;

import mova.game.asset.ResourceCache;
import mova.game.core.FatalError;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.AttributedCharacterIterator;
import java.util.Map;

@Component
public final class FontCache extends ResourceCache<Font> {

    @Override
    protected String getResourceRoot() {
        return "/fonts/";
    }

    @Override
    protected Font extract(URL url) throws IOException {
        try (InputStream resourceContent = url.openStream()) {
            Font font = Font.createFont(Font.TRUETYPE_FONT, resourceContent);
            register(font);
            return font;
        } catch (FontFormatException e) {
            throw new FatalError("Impossible de charger la police: " + url, e);
        }
    }

    @Override
    protected Font postExtract(String resourceName, Font font) {
        return new FontResource(font, resourceName);
    }

    public Font getResource(String resourceName, int style, float size) {
        String deriveFontName = resourceName + "_" + style + "_" + size;

        return resources.computeIfAbsent(deriveFontName, fontName -> {
            Font defaultFont = super.getResource(resourceName);
            Font deriveFont = defaultFont.deriveFont(style, size);
            register(deriveFont);

            return postExtract(fontName, deriveFont);
        });
    }

    public Font getResource(Font baseFont, int style, float size) {
        String baseFontName = baseFont instanceof FontResource ? ((FontResource) baseFont).getResourceName() : baseFont.getName();

        return resources.computeIfAbsent(baseFontName + "_" + style + "_" + size, fontName -> {
            Font deriveFont = new FontResource(baseFont.deriveFont(style, size), fontName);
            register(deriveFont);

            return postExtract(fontName, deriveFont);
        });
    }

    public Font getResource(Font baseFont, Map<? extends AttributedCharacterIterator.Attribute, ?> attributes) {
        String baseFontName = baseFont instanceof FontResource ? ((FontResource) baseFont).getResourceName() : baseFont.getName();

        StringBuilder builder = new StringBuilder(baseFontName);
        for (AttributedCharacterIterator.Attribute attribute : attributes.keySet()) {
            builder.append("_").append(attribute);
        }

        return resources.computeIfAbsent(builder.toString(), fontName -> {
            Font deriveFont = new FontResource(baseFont.deriveFont(attributes), fontName);
            register(deriveFont);

            return postExtract(fontName, deriveFont);
        });
    }

    private void register(Font font) {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        ge.registerFont(font);
    }
}
