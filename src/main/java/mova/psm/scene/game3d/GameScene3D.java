package mova.psm.scene.game3d;

import mova.game.core.SceneOrchestrator;
import mova.game.core.ecs.AbstractGameComponent;
import mova.game.core.ecs.GameObject;
import mova.game.graphics.MovaGraphics;
import mova.game.graphics.component.GameButton;
import mova.game.graphics.component.GamePanel;
import mova.game.graphics.component.GameWindow;
import mova.game.graphics.component.IconSet;
import mova.game.graphics.component.event.GameActionListener;
import mova.game.graphics3D.MovaPolygonRenderer;
import mova.game.graphics3D.PolygonGroup;
import mova.game.graphics3D.PolygonRenderer;
import mova.game.graphics3D.math3D.Camera;
import mova.game.graphics3D.math3D.Transform3D;
import mova.game.graphics3D.math3D.Vector3D;
import mova.game.input.GameAction;
import mova.game.input.GameActionMapping;
import mova.game.input.GameInputsListener;
import mova.psm.asset.ImageIconCache;
import mova.psm.asset.PolygonGroupCache;
import mova.psm.ecs.PSMGameObject;
import mova.psm.scene.HomeScene;
import mova.psm.scene.PSMAbstractScene;
import mova.psm.scene.SceneUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class GameScene3D extends PSMAbstractScene implements GameActionListener {

    private final SceneOrchestrator sceneOrchestrator;
    private final PolygonGroupCache polygonGroupCache;
    private final ImageIconCache imageIconCache;
    private final GameInputsListener gameInputsListener;

    private Map<Class<? extends GameMode>, GameMode> modes;

    private Class<? extends GameMode> currentModeClass;

    private GameButton quitButton;
    private GameAction frameRateToggle;

    protected PolygonRenderer polygonRenderer;

    private List<GameObject> gameObjects;

    @Autowired
    public GameScene3D(
            SceneOrchestrator sceneOrchestrator,
            GameWindow gameWindow,
            PolygonGroupCache polygonGroupCache,
            ImageIconCache imageIconCache,
            GameInputsListener gameInputsListener) {
        super(gameWindow);

        this.sceneOrchestrator = sceneOrchestrator;
        this.polygonGroupCache = polygonGroupCache;
        this.imageIconCache = imageIconCache;
        this.gameInputsListener = gameInputsListener;
    }

    @Override
    public void start() {
        Transform3D camera = new Transform3D(38, 230, 650);
        camera.getRotation().addAngleX(-0.25f);
        camera.getRotation().addAngleY(-0.12f);

        Camera cameo = new Camera(camera, gameWindow);


        polygonRenderer = new MovaPolygonRenderer(cameo);

        gameObjects = new ArrayList<>();

        createSea();
        createShips();


        ImageIcon iconRollover = imageIconCache.getResource("menu/quit.png");
        IconSet iconSet = SceneUtils.createIconSet(gameWindow, iconRollover);

        quitButton = SceneUtils.createIconButton("Quit", iconSet);
        quitButton.addActionListener(this);

        GamePanel buttons = new GamePanel(new FlowLayout());
        buttons.add(quitButton);

        gameWindow.addContents(buttons);

        GameActionMapping parentMapping = new GameActionMapping();
        parentMapping.mapToKey(quitButton.getGameAction(), KeyEvent.VK_ESCAPE);

        frameRateToggle = parentMapping.mapToKey("Frame Rate", KeyEvent.VK_R);

        modes = new HashMap<>();
        modes.put(NullMode.class, new NullMode());
        modes.put(NormalMode.class, new NormalMode(parentMapping, cameo, polygonRenderer));
        modes.put(RatedMode.class, new RatedMode(parentMapping, cameo, polygonRenderer));

        currentModeClass = NullMode.class;
        setMode(NormalMode.class);

        gameInputsListener.listenTo(gameWindow);

        super.start();
    }

    private void createSea() {
        PolygonGroup seaPolygons = polygonGroupCache.getResource("sea.obj");

        PSMGameObject sea = new PSMGameObject("sea");
        sea.addComponent(seaPolygons);

        gameObjects.add(sea);
    }

    private void createShips() {
        PolygonGroup shipPolygons = polygonGroupCache.getResource("ship.obj");

        PSMGameObject ship = new PSMGameObject("ship");
        ship.addComponent(shipPolygons);

        for (int i = 0; i < 5; i++) {
            PSMGameObject g = ship.copy();
            PolygonGroup polygonGroup = g.getComponent(PolygonGroup.class);
            polygonGroup.add(new Vector3D(0, 0, i*500f));
            g.addComponent(new AbstractGameComponent() {
                @Override
                public void update(long dt) {
                    getGameObject().applyIfPresent(PolygonGroup.class, p -> p.add(new Transform3D(.1f*dt, 0, 0)));
                }
            });
            gameObjects.add(g);
        }
    }

    @Override
    public void release() {
        gameInputsListener.detach();

        gameWindow.clearContent();
        quitButton = null;

        modes.clear();
        modes = null;
    }

    @Override
    public void update(long dt) {
        modes.get(currentModeClass).update(dt, gameObjects);

        if (frameRateToggle.isPressed()) {
            if (!currentModeClass.equals(RatedMode.class)) {
                setMode(RatedMode.class);
            } else {
                setMode(NormalMode.class);
            }
        }

        if (quitButton.isPressed()) {
            sceneOrchestrator.setScene(HomeScene.class);
        }
    }

    @Override
    public void draw(MovaGraphics graphics) {
        modes.get(currentModeClass).draw(graphics, gameObjects);

        gameWindow.paintContent(graphics);
    }

    private void setMode(Class<? extends GameMode> modeClass) {
        if (!modeClass.equals(currentModeClass)) {
            GameMode oldMode = modes.get(currentModeClass);
            oldMode.getGameActionMapping().resetAllGameActions();

            currentModeClass = modeClass;

            GameMode newMode = modes.get(currentModeClass);
            newMode.entry();
            gameInputsListener.setGameActionMapping(newMode.getGameActionMapping());
        }
    }

}
