package mova.psm.input;

import mova.game.input.GameInputsListener;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class PSMInputConfig {

    @PostConstruct
    private void init() {
        LoggerFactory.getLogger(PSMInputConfig.class).debug("Initialisation du GameInputsListener");
    }

    @Bean
    public GameInputsListener gameInputsListener() {
        return new GameInputsListener();
    }
}
