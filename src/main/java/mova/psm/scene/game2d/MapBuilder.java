package mova.psm.scene.game2d;

import mova.Settings;
import mova.game.graphics.*;
import mova.game.graphics.component.GameWindow;
import mova.game.model.DescriptionProvider;
import mova.game.model.ModelContainer;
import mova.game.physics.RigidBody;
import mova.psm.asset.ImageCache;
import mova.psm.ecs.PSMGameObject;
import mova.psm.map.IslandModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Component
final class MapBuilder {

    private static final BasicStroke DASHED_STROKE = new BasicStroke(20, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10f, new float[]{40}, 0);
    private static final Color DASHED_COLOR = new Color(34, 12, 50);
    private static final BasicStroke DASHED_STROKE2 = new BasicStroke(20, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10f, new float[]{0, 40, 40, 0}, 0);
    private static final Color DASHED_COLOR2 = Color.WHITE;

    private final ImageCache imageCache;
    private final GameWindow gameWindow;

    @Autowired
    MapBuilder(ImageCache imageCache, GameWindow gameWindow) {
        this.imageCache = imageCache;
        this.gameWindow = gameWindow;
    }


    PSMGameObject createMap(Rectangle mapShape) {
        PSMGameObject seaMap = new PSMGameObject("map");
        BufferedImage seaTile = imageCache.getResource("seamless-sea-bg.jpg");

        seaMap.addComponent(new RenderableGroup());

        RenderableGroup shapes = seaMap.getComponent(RenderableGroup.class);
        shapes.addShapeRenderer("Water", new TextureShapeRenderer(mapShape, seaTile, gameWindow));
        shapes.addShapeRenderer("Border1", new SolidShape(mapShape, DASHED_STROKE, DASHED_COLOR));
        shapes.addShapeRenderer("Border2", new SolidShape(mapShape, DASHED_STROKE2, DASHED_COLOR2));

        return seaMap;
    }

    List<PSMGameObject> createIslands(List<Shape> islandModels) {
        List<PSMGameObject> islands = new ArrayList<>();

        for (Shape islandShape : islandModels) {
            PSMGameObject island = terraform(islandShape);
            decorate(island);
            indexIsland(island);

            islands.add(island);
        }

        return islands;
    }

    private PSMGameObject terraform(Shape islandShape) {
        PSMGameObject island = new PSMGameObject("island_" + System.currentTimeMillis());
        island.addComponent(new ModelContainer(new IslandModel()));
        island.addComponent(new Transform()).setZIndex(1);
        island.addComponent(new RigidBody(islandShape));

        return island;
    }

    private void decorate(PSMGameObject island) {
        Shape islandShape = island.getComponent(RigidBody.class).getBodyShape();
        island.addComponent(new RenderableGroup());

        RenderableGroup shapes = island.getComponent(RenderableGroup.class);
        BufferedImage islandTile = imageCache.getResource("sand-seamless.jpg");
        shapes.addShapeRenderer("Sand", new TextureShapeRenderer(islandShape, islandTile, gameWindow));
        shapes.addShapeRenderer("Shore", new SolidShape(islandShape, Settings.DEFAULT_STROKE, Color.BLACK));
    }

    private void indexIsland(PSMGameObject island) {
        // TODO le description provider pourrait avoir des objets en cache qui serait plus simplement accessible (comme le IslandModel pour les îles)
        DescriptionProvider provider = new DescriptionProvider();
        provider.createCollector("Nom", go -> {
            IslandModel model = go.getComponent(ModelContainer.class).getModel(IslandModel.class);
            return model.isWild() ? "Île sauvage" : "Île de l'amiral " + model.getOwner().getName();
        });
        provider.createCollector("Trésor", go -> {
            IslandModel model = go.getComponent(ModelContainer.class).getModel(IslandModel.class);
            return model.getTreasures().stream().map(Object::toString).collect(Collectors.joining(", "));
        });

        island.addComponent(provider);
    }
}
