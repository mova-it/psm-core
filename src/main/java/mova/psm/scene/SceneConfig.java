package mova.psm.scene;

import mova.game.core.SceneOrchestrator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SceneConfig {

    @Bean
    public SceneOrchestrator sceneOrchestrator(ApplicationContext applicationContext) {
        return new SceneOrchestrator(applicationContext::getBean);
    }

}
