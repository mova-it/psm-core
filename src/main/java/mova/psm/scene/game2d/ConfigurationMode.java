package mova.psm.scene.game2d;

import mova.Settings;
import mova.game.core.ecs.GameObject;
import mova.game.geom.Segment;
import mova.game.graphics.RenderableGroup;
import mova.game.graphics.SolidShape;
import mova.game.graphics.component.GameWindow;
import mova.game.input.GameInputsListener;
import mova.game.physics.RigidBody;

import java.awt.*;
import java.util.Iterator;
import java.util.List;

class ConfigurationMode extends BaseGameMode {
    private static final String FORBIDDEN_CROSS_LINE_1_NAME = "Forbidden cross line 1";
    private static final String FORBIDDEN_CROSS_LINE_2_NAME = "Forbidden cross line 2";
    static final String FORBIDDEN_CROSS_NAME = "Forbidden cross";

    private final List<GameModePhase<ConfigurationMode>> phases;
    private Iterator<GameModePhase<ConfigurationMode>> phasesIteraor;
    private GameModePhase<ConfigurationMode> currentPhase = null;

    ConfigurationMode(GameScene scene, GameWindow gameWindow, GameInputsListener gameInputsListener) {
        super(scene, gameWindow, gameInputsListener);

        phases = GameModePhase.createPhasesSequence(this, PlayerSelectionPhase.class, IslandsPlacementPhase.class, HomeIslandSelectionPhase.class, TreasurePlacementPhase.class);
    }

    @Override
    public void entry() {
        super.entry();

        phasesIteraor = phases.iterator();

        nextPhase();
    }

    @Override
    public void update(long dt) {
        super.update(dt);

        currentPhase.update(dt);
    }

    @Override
    public Class<? extends GameMode> handleModeChange() {
        if (phasesIteraor == null) return NormalMode.class;

        return null;
    }

    void nextPhase() {
        if (currentPhase != null) currentPhase.release();

        if (phasesIteraor.hasNext()) {
            currentPhase = phasesIteraor.next();
            currentPhase.entry();
        } else {
            phasesIteraor = null;
        }
    }

    static void removeForbiddenCross(GameObject island) {
        RenderableGroup group = island.getComponent(RenderableGroup.class);
        group.removeShapeRenderer(FORBIDDEN_CROSS_NAME);
    }

    static void addForbiddenCross(GameObject island, boolean visible) {
        Rectangle bounds = island.getComponent(RigidBody.class).getBodyShape().getBounds();

        RenderableGroup forbiddenCross = new RenderableGroup(visible);
        forbiddenCross.addShapeRenderer(FORBIDDEN_CROSS_LINE_1_NAME, new SolidShape(new Segment(bounds.getMinX(), bounds.getMinY(), bounds.getMaxX(), bounds.getMaxY()), Settings.DEFAULT_STROKE, Color.RED));
        forbiddenCross.addShapeRenderer(FORBIDDEN_CROSS_LINE_2_NAME, new SolidShape(new Segment(bounds.getMaxX(), bounds.getMinY(), bounds.getMinX(), bounds.getMaxY()), Settings.DEFAULT_STROKE, Color.RED));

        RenderableGroup mainGroup = island.getComponent(RenderableGroup.class);
        mainGroup.addShapeRenderer(FORBIDDEN_CROSS_NAME, forbiddenCross);
    }

}
