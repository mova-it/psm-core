package mova.psm.event;

import mova.game.event.EventSystem;
import mova.game.event.Observer;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.Collection;

@Configuration
public class PSMEventConfig {

    @PostConstruct
    private void init() {
        LoggerFactory.getLogger(PSMEventConfig.class).debug("Initialisation de l'observateur d'évènement");
    }

    @Autowired
    public PSMEventConfig(Collection<Observer> observers) {
        EventSystem.addObservers(observers);
    }
}
