package mova.psm;

import mova.debug.BenchDataObserver;
import mova.psm.core.PSMGameCore;
import mova.util.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

@SpringBootApplication(scanBasePackages = "mova")
public class PSMConfig {

    @Value("${psm.thread.name:}")
    private String name;

    @EventListener
    public void onApplicationReadyEvent(ApplicationReadyEvent event) {
        Runnable runnable = () -> event.getApplicationContext().getBean(PSMGameCore.class).run();

        if (StringUtils.isNotBlank(name)) new Thread(runnable, name).start();
        else runnable.run();
    }

    public static void main(String[] args) {
        SpringApplicationBuilder builder = new SpringApplicationBuilder(PSMConfig.class);
        builder.headless(false).run(args);

        BenchDataObserver.resume();
    }

}