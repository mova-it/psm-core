package mova.psm.scene.game2d;


import mova.Settings;
import mova.game.core.ecs.AbstractGameComponent;
import mova.game.graphics.MovaGraphics;
import mova.util.StringUtils;

import java.awt.*;

// TODO Je pense que ca peut devenir un TextRenderer
public final class SplashScreen extends AbstractGameComponent {

    private Rectangle zone = new Rectangle();
    private Font font = Settings.DIALOG_FONT;
    private Color color = Settings.DEFAULT_TRANSPARENT_COLOR;
    private String text = StringUtils.EMPTY;

    private Point textLocation = null; // dirty flag

    public void setSize(Dimension size) {
        this.zone = new Rectangle(new Point(), size);

        textLocation = null;
    }

    public void setFont(Font font) {
        this.font = font;

        textLocation = null;
    }

    public void setText(String text) {
        this.text = text;

        textLocation = null;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public void draw(MovaGraphics graphics) {

        if (textLocation == null) {
            // Get the FontMetrics
            FontMetrics metrics = graphics.getFontMetrics(font);

            textLocation = new Point();
            // Determine the X coordinate for the text
            textLocation.x = (zone.width - metrics.stringWidth(text)) / 2;
            // Determine the Y coordinate for the text (note we add the ascent, as in java 2d 0 is top of the screen)
            textLocation.y = ((zone.height - metrics.getHeight()) / 2) + metrics.getAscent();
        }

        // TODO cette phase est très longue?! remplir l'écran prend trop de temps
        graphics.setColor(color);
        graphics.fill(zone);

        graphics.setColor(Color.ORANGE);
        graphics.setFont(font);
        graphics.drawString(text, textLocation.x, textLocation.y);
    }
}
