package mova.psm.scene.game2d;

import mova.Settings;
import mova.game.core.ecs.GameObject;
import mova.game.geom.Locator;
import mova.game.graphics.RenderableGroup;
import mova.game.graphics.ShapeRenderer;
import mova.game.graphics.SolidShape;
import mova.game.graphics.Transform;
import mova.game.graphics.geom.ShapeUtils;
import mova.game.input.GameAction;
import mova.game.input.MouseUserAction;
import mova.game.model.ModelContainer;
import mova.game.physics.RigidBody;
import mova.psm.ecs.PSMGameObject;
import mova.psm.map.IslandModel;
import mova.psm.measure.Ruler;
import mova.psm.ship.Range;

import java.awt.*;
import java.awt.geom.Area;

class IslandsPlacementPhase extends GameModePhase<ConfigurationMode> {

    private static final float TROIS_L = 3f * Ruler.getRangeLength(Range.L);
    private static final float SIX_L = 6f * Ruler.getRangeLength(Range.L);
    private static final Color FORBIDDEN_AREA_COLOR = new Color(.5f, 0, 0, .4f);
    private static final Color ALLOW_AREA_COLOR = new Color(0, .5f, 0, .4f);
    private static final String FORBIDDEN_ZONE_NAME = "Forbidden Zone";
    private static final String ALLOW_ZONE_NAME = "Allow Zone";

    private GameAction putObject;

    private GameObject[] islands;
    private int islandIndex;

    private PSMGameObject controlZones;
    private Area allowArea;
    private Area forbiddenArea;

    protected IslandsPlacementPhase(ConfigurationMode mode) {
        super(mode);
    }

    @Override
    void entry() {
        putObject = mode.gameActionMapping.mapToMouse("Put Object", MouseUserAction.LEFT_BUTTON);

        islands = mode.scene.getGameObjects().findGameObjects(gameObject -> {
            ModelContainer modelContainer = gameObject.getComponent(ModelContainer.class);
            return modelContainer != null && modelContainer.hasModel(IslandModel.class);
        });

        GameObject map = mode.scene.getGameObjects().findGameObject("map");
        Shape mapShape = map.getComponent(RenderableGroup.class).getShapeRenderer("Water", ShapeRenderer.class).getShape();

        allowArea = new Area();

        forbiddenArea = new Area(mapShape);
        Shape shorterMapShape = ShapeUtils.offset(mapShape, -TROIS_L);
        forbiddenArea.subtract(new Area(shorterMapShape));

        controlZones = new PSMGameObject("Control Zone");
        controlZones.addComponent(new RenderableGroup());

        RenderableGroup group = controlZones.getComponent(RenderableGroup.class);
        group.addShapeRenderer(ALLOW_ZONE_NAME, new SolidShape(allowArea, ALLOW_AREA_COLOR, Settings.DEFAULT_STROKE, Color.BLACK));
        group.addShapeRenderer(FORBIDDEN_ZONE_NAME, new SolidShape(forbiddenArea, FORBIDDEN_AREA_COLOR, Settings.DEFAULT_STROKE, Color.BLACK));
        group.getShapeRenderers().forEach(shapeRenderer -> shapeRenderer.setMutable(true));
        mode.scene.addGameObject(controlZones);
        controlZones.show();

        setCurrentIsland(0);
    }

    private long forbiddenCrossTime = 0;

    @Override
    public void update(long dt) {
        GameObject island = islands[islandIndex];

        Locator mouseLocation = mode.gameInputsListener.getMouseLocation();
        Locator worldLocation = mode.scene.getCamera().getScreenProjection().inverseTransform(mouseLocation);
        island.getComponent(Transform.class).setToTranslation(worldLocation.getX(), worldLocation.getY());

        if (forbiddenCrossTime > 0) forbiddenCrossTime -= Math.min(dt, forbiddenCrossTime);
        else if (forbiddenCrossTime == 0) {
            RenderableGroup group = island.getComponent(RenderableGroup.class);
            group.getShapeRenderer(ConfigurationMode.FORBIDDEN_CROSS_NAME).setVisible(false);
            forbiddenCrossTime = -1;
        }

        if (putObject.isPressed()) {

            if (isCurrentIslandPlaceAllowed()) {

                if (islandIndex < islands.length - 1) {
                    mode.scene.nextAdmiral();

                    mergeControlZones(island);
                    island.getComponent(RenderableGroup.class).removeShapeRenderer(FORBIDDEN_ZONE_NAME);

                    setCurrentIsland(++islandIndex);
                } else {
                    mode.nextPhase();
                }
            } else {
                RenderableGroup group = island.getComponent(RenderableGroup.class);
                group.getShapeRenderer(ConfigurationMode.FORBIDDEN_CROSS_NAME).setVisible(true);

                forbiddenCrossTime = 500;
            }
        }
    }

    @Override
    void release() {
        cleanComponents();
    }

    private void setCurrentIsland(int index) {
        islandIndex = index;

        PSMGameObject island = (PSMGameObject) islands[islandIndex];
        Shape forbiddenZone = createZone(island, TROIS_L);

        RenderableGroup group = island.getComponent(RenderableGroup.class);
        group.addShapeRenderer(FORBIDDEN_ZONE_NAME, new SolidShape(forbiddenZone, FORBIDDEN_AREA_COLOR, Settings.DEFAULT_STROKE, Color.BLACK));

        ConfigurationMode.addForbiddenCross(island, false);

        island.show();
    }

    private Shape createZone(GameObject island, float length) {
        Shape islandBody = island.getComponent(RigidBody.class).getBodyShape();

        return ShapeUtils.offset(islandBody, length);
    }

    private void mergeControlZones(GameObject island) {
        Shape forbiddenZone = createZone(island, TROIS_L);
        forbiddenZone = island.getComponent(Transform.class).apply(forbiddenZone);
        forbiddenArea.add(new Area(forbiddenZone));

        Shape allowZone = createZone(island, SIX_L);
        allowZone = island.getComponent(Transform.class).apply(allowZone);
        allowArea.add(new Area(allowZone));
        allowArea.subtract(forbiddenArea);
    }

    private void cleanComponents() {
        for (GameObject island : islands) {
            RenderableGroup group = island.getComponent(RenderableGroup.class);
            group.removeShapeRenderer(FORBIDDEN_ZONE_NAME);

            ConfigurationMode.removeForbiddenCross(island);
        }
        controlZones.destroy();
    }

    private boolean isCurrentIslandPlaceAllowed() {
        GameObject island = islands[islandIndex];
        Area islandArea = getIslandArea(island);

        // Est ce que l'ile est dans la zone interdite
        if (ShapeUtils.intersect(forbiddenArea, islandArea)) return false;

        // La première île peut être placé en dehors de la zone autorisée
        if (islandIndex == 0) return true;

        // Est ce qu'une autre ile est dans la zone interdite de l'ile courrante
        Area islandForbiddenArea = getIslandForbiddenArea(island);
        if (isIslandTooCloseFromAnother(islandForbiddenArea)) return false;

        // Est ce que l'ile est dans la zone autorisée
        return ShapeUtils.intersect(allowArea, islandArea);
    }

    private boolean isIslandTooCloseFromAnother(Area islandForbiddenArea) {
        for (int i = islandIndex - 1; i >= 0; i--) {
            GameObject otherIsland = islands[i];

            // Est ce que les précédentes iles sont dans la zone interdite de l'ile courrante
            Area otherIslandArea = getIslandArea(otherIsland);
            if (ShapeUtils.intersect(islandForbiddenArea, otherIslandArea)) return true;
        }

        return false;
    }

    private Area getIslandArea(GameObject island) {
        Shape islandBody = island.getComponent(RigidBody.class).getBodyShape();
        islandBody = island.getComponent(Transform.class).apply(islandBody);
        return new Area(islandBody);
    }

    private Area getIslandForbiddenArea(GameObject island) {
        Shape forbiddenZone = island.getComponent(RenderableGroup.class).getShapeRenderer(FORBIDDEN_ZONE_NAME, ShapeRenderer.class).getShape();
        forbiddenZone = island.getComponent(Transform.class).apply(forbiddenZone);
        return new Area(forbiddenZone);
    }
}
