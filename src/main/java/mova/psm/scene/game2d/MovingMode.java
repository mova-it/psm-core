package mova.psm.scene.game2d;

import mova.game.graphics.component.GameWindow;
import mova.game.input.GameInputsListener;
import mova.psm.core.ship.Pilot;
import mova.psm.ecs.PSMGameObject;

final class MovingMode extends BaseGameMode {

    private PSMGameObject currentShip;

    MovingMode(GameScene scene, GameWindow gameWindow, GameInputsListener gameInputsListener) {
        super(scene, gameWindow, gameInputsListener);
    }

    @Override
    public void entry() {
        super.entry();

        ((PSMGameObject) scene.getGameObjects().findGameObject(GameScene.MARKER_GAME_OBJECT_NAME)).disable();

        currentShip = scene.getSelectedGameObject();
        currentShip.activate();
        currentShip.set("played", true);
        scene.updateUI();
    }

    @Override
    public Class<? extends GameMode> handleModeChange() {
        Pilot pilot = currentShip.getComponent(Pilot.class);
        if (!pilot.hasCourses()) {
            currentShip.deactivate();
            return NormalMode.class;
        }

        return null;
    }
}
