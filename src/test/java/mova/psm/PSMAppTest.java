package mova.psm;

import mova.lang.ThreadUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PSMConfig.class)
@ContextConfiguration(loader = HeadlessSpringBootContextLoader.class)
@ActiveProfiles("test")
public class PSMAppTest {

    @Autowired
    private ApplicationContext appContext;

    @Test
    public void startPSM() {

        try {
            LoggerFactory.getLogger(getClass()).debug("Chargement du thread");

            Thread t = new Thread(() -> {

                SpringApplication.exit(appContext, () -> {
                    LoggerFactory.getLogger(getClass()).debug("Extinction du jeu");
                    ThreadUtils.lazilyExit();

                    return 0;
                });
            });


            ThreadUtils.takeANap(20000);

            t.start();

            LoggerFactory.getLogger(getClass()).debug("Exécution du thread");
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }


}
