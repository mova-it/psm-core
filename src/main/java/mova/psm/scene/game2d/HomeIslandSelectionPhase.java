package mova.psm.scene.game2d;

import mova.Settings;
import mova.game.core.ecs.GameObject;
import mova.game.geom.Locator;
import mova.game.geom.Segment;
import mova.game.graphics.RenderableGroup;
import mova.game.graphics.SolidShape;
import mova.game.graphics.Transform;
import mova.game.graphics.geom.GeometryUtils;
import mova.game.input.GameAction;
import mova.game.input.MouseUserAction;
import mova.game.model.ModelContainer;
import mova.game.physics.RigidBody;
import mova.lang.MathUtils;
import mova.psm.admiral.Admiral;
import mova.psm.ecs.PSMGameObject;
import mova.psm.map.IslandModel;
import mova.trigo.TrigoUtils;
import mova.util.RandomUtils;
import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MessageFormatter;

import java.awt.*;
import java.text.MessageFormat;
import java.util.List;
import java.util.*;

class HomeIslandSelectionPhase extends GameModePhase<ConfigurationMode> {

    private GameAction selectIsland;

    private PSMGameObject informationObject;

    private int currentPlayer;
    private int choosingPlayer;
    private Map<GameObject, Long> forbiddenIslands;

    protected HomeIslandSelectionPhase(ConfigurationMode mode) {
        super(mode);
    }

    @Override
    void entry() {
        selectIsland = mode.gameActionMapping.mapToMouse("Select island", MouseUserAction.LEFT_BUTTON);

        SplashScreen splashScreen = new SplashScreen();
        splashScreen.setSize(mode.gameWindow.getSize());
        splashScreen.setFont(Settings.DIALOG_FONT.deriveFont(50f));

        informationObject = new PSMGameObject("INFORMATION");
        informationObject.addComponent(splashScreen);
        mode.scene.addGameObject(informationObject);
        informationObject.show();

        setPlayer(0);
        updateInformation();

        forbiddenIslands = new HashMap<>();
    }

    @Override
    void update(long dt) {
        /*The last player chooses which island will be the home island of the first player. The first player
places his or her ships so that their bows (fronts of the ships) touch that island. The first player then
chooses a different island to be the second player's home island, and that player places his or her
ships so that their bows touch that island. The remaining player's home islands are chosen in order in
this manner. The remaining islands are called wild islands.*/

        if (selectIsland.isPressed()) {
            Locator mouseLocation = mode.gameInputsListener.getMouseLocation();
            Locator mouseWorldLocation = mode.scene.getCamera().getScreenProjection().inverseTransform(mouseLocation);
            mode.scene.getGameObjects().findGameObject(mouseWorldLocation, island -> {
                ModelContainer modelContainer = island.getComponent(ModelContainer.class);
                if (modelContainer != null) {
                    IslandModel islandModel = modelContainer.getModel(IslandModel.class);
                    if (islandModel != null) {
                        if (islandModel.isWild()) {
                            addOwner(island, islandModel);
                        } else {
                            markAsForbidden(island);
                        }
                    }
                }
            });
        }

        updateForbiddenCross(dt);
    }

    private void markAsForbidden(GameObject island) {
        ConfigurationMode.addForbiddenCross(island, true);

        forbiddenIslands.put(island, 500L);
    }

    private void addOwner(GameObject island, IslandModel islandModel) {
        Admiral currentAdmiral = mode.scene.admirals.get(currentPlayer);
        islandModel.setOwner(currentAdmiral);
        addRandomFlag(island);
        setPlayer(currentPlayer + 1);
        placeShips(currentAdmiral, island);

        if (currentPlayer >= mode.scene.admirals.size()) mode.nextPhase();
        else updateInformation();
    }

    private void updateForbiddenCross(long dt) {
        if (!forbiddenIslands.isEmpty()) {
            Iterator<Map.Entry<GameObject, Long>> forbiddenIslandIterator = forbiddenIslands.entrySet().iterator();
            while (forbiddenIslandIterator.hasNext()) {
                Map.Entry<GameObject, Long> forbiddenIsland = forbiddenIslandIterator.next();
                long delay = forbiddenIsland.getValue();
                delay -= Math.min(dt, delay);

                if (delay > 0) forbiddenIsland.setValue(delay);
                else {
                    ConfigurationMode.removeForbiddenCross(forbiddenIsland.getKey());
                    forbiddenIslandIterator.remove();
                }
            }
        }
    }

    private void setPlayer(int i) {
        currentPlayer = i;
        choosingPlayer = MathUtils.euclidianModulo(currentPlayer - 1, mode.scene.admirals.size());
    }

    public static Locator[] locators = null;

    private void placeShips(Admiral admiral, GameObject island) {
        // TODO il n'y a rien qui empêche un bateau d'être dessiné à l'extérieur de la map
        // TODO du coup il n'y a pas non plus de détection de collision avec les bords de la map.
        List<PSMGameObject> ships = mode.scene.getShips(admiral);
        for (PSMGameObject ship : ships) {
            Transform shipTransform = ship.getComponent(Transform.class);

            Shape islandShape = island.getComponent(RigidBody.class).getBodyShape();
            Rectangle islandShapeBounds = islandShape.getBounds();
            float diagLength = (float) Math.hypot(islandShapeBounds.width, islandShapeBounds.height);

            Transform islandTransform = island.getComponent(Transform.class);
            //TODO Il faudra placer les bateau accosté à une île et correctement orienté

            locators = new Locator[41];
            for (int i = 0; i < locators.length/2; i++) {
                double theta = TrigoUtils.DEUX_PI * Math.random();
                double circleX = diagLength * Math.cos(theta);
                double circleY = diagLength * Math.sin(theta);
                Locator circlePoint = new Locator(circleX, circleY);

                Locator closestLocator = GeometryUtils.getClosestPointFrom(islandShape, circlePoint);

                locators[2*i] = islandTransform.apply(circlePoint);
                locators[2*i + 1] = islandTransform.apply(closestLocator);
            }

            locators[40] = islandTransform.apply(new Locator(islandShapeBounds.getCenterX(), islandShapeBounds.getCenterY()));

            do {
                shipTransform.setTo(islandTransform);

                double theta = TrigoUtils.DEUX_PI*Math.random();
                double circleX = diagLength*Math.cos(theta);
                double circleY = diagLength*Math.sin(theta);
                // TODO le problème est que mes rigidbody shape n'ont pas la même origine (le coin inférieur gauche pour les bateaux le point central gauche pour les îles, etc.)
                //  il faut régler ce problème => je comprends maintenant pourquoi pour uniformiser les choses on considère que les objet sont centrés

                shipTransform.addTranslation(circleX, circleY);
            } while (mode.scene.getGameObjects().findGameObjects(gameObject -> gameObject.hasComponent(RigidBody.class) && gameObject.getComponent(RigidBody.class).isColliding(ship)).length > 0);

            ship.show();
        }
    }

    private void updateInformation() {
        informationObject.getComponent(SplashScreen.class).setText(mode.scene.admirals.get(choosingPlayer).getName() + " choisi l'île de base de " + mode.scene.admirals.get(currentPlayer).getName());
    }

    private void addRandomFlag(GameObject island) {
        Shape islandShape = island.getComponent(RigidBody.class).getBodyShape();
        Rectangle bounds = islandShape.getBounds();

        Shape flagShape = new Rectangle((int) bounds.getCenterX(), (int) bounds.getCenterY(), 100, 80);
        Shape flagStaffShape = new Segment(bounds.getCenterX(), bounds.getCenterY() - 80 , bounds.getCenterX(), bounds.getCenterY());

        RenderableGroup flagGroup = new RenderableGroup();
        flagGroup.addShapeRenderer("Flag", new SolidShape(flagShape, getRandomColor(), Settings.DEFAULT_STROKE, Color.BLACK));
        flagGroup.addShapeRenderer("FlagStaff", new SolidShape(flagStaffShape, Settings.DEFAULT_STROKE, Color.BLACK));

        RenderableGroup group = island.getComponent(RenderableGroup.class);
        group.addShapeRenderer("Flag", flagGroup);
    }

    // TODO vraiment pas bon parce que:
    //  - la couleur devrait être lié à l'amiral?
    //  - il faut des couleurs différentes par amiral
    //  - il faut assez de couleur pour tous les amiraux
    private static final List<Color> colors = Arrays.asList(Color.RED, Color.BLUE, Color.MAGENTA, Color.YELLOW, Color.GREEN, Color.GRAY, Color.ORANGE);

    private static Color getRandomColor() {
        return RandomUtils.randElement(colors);
    }

    @Override
    void release() {
        if (!forbiddenIslands.isEmpty()) {
            for (GameObject forbiddenIsland : forbiddenIslands.keySet()) {
                RenderableGroup group = forbiddenIsland.getComponent(RenderableGroup.class);
                group.removeShapeRenderer(ConfigurationMode.FORBIDDEN_CROSS_NAME);
            }

            forbiddenIslands.clear();
        }

        informationObject.destroy();
    }

}
