package mova.psm.scene.game2d;

import mova.game.geom.Locator;
import mova.game.graphics.RenderableGroup;
import mova.game.graphics.SolidShape;
import mova.game.graphics.Transform;
import mova.game.graphics.component.GameWindow;
import mova.game.input.GameAction;
import mova.game.input.GameInputsListener;
import mova.game.input.MouseUserAction;
import mova.game.model.ModelContainer;
import mova.game.view.Camera;
import mova.psm.core.ship.Pilot;
import mova.psm.ecs.PSMGameObject;
import mova.psm.measure.AngleConstraints;
import mova.psm.measure.Ruler;
import mova.psm.measure.RulerDimensioner;
import mova.psm.ship.MovementPool;
import mova.psm.ship.Range;
import mova.psm.ship.ShipModel;
import mova.psm.ship.ShipSide;

import java.awt.event.KeyEvent;

final class MovePlaningMode extends BaseGameMode {

    private final GameAction portsideAction;
    private final GameAction starboardAction;
    private final GameAction storeCourseAction;
    private final GameAction cancelCourseAction;
    private final GameAction validateCoursesAction;
    private final GameAction cancelModeAction;

    private PSMGameObject currentShip;
    private AngleConstraints angleConstraints = null;
    private PSMGameObject ghost;

    MovePlaningMode(GameScene scene, GameWindow gameWindow, GameInputsListener gameInputsListener) {
        super(scene, gameWindow, gameInputsListener);

        portsideAction = gameActionMapping.mapToKey("Choose portside", KeyEvent.VK_LEFT);
        starboardAction = gameActionMapping.mapToKey("Choose starboard", KeyEvent.VK_RIGHT);
        storeCourseAction = gameActionMapping.mapToMouse("Store course", MouseUserAction.LEFT_BUTTON);
        cancelCourseAction = gameActionMapping.mapToKey("Cancel course", KeyEvent.VK_BACK_SPACE);
        gameActionMapping.mapToMouse(cancelCourseAction, MouseUserAction.REAR_BUTTON);
        validateCoursesAction = gameActionMapping.mapToKey("Validate courses", KeyEvent.VK_SPACE);

        cancelModeAction = gameActionMapping.mapToKey("Cancel mode", KeyEvent.VK_M);
    }

    @Override
    public void entry() {
        super.entry();

        currentShip = scene.getSelectedGameObject();

        ShipModel currentShipModel = currentShip.getComponent(ModelContainer.class).getModel(ShipModel.class);
        currentShipModel.getMovementPool().reset();

        ghost = currentShip.copy();
        ghost.addComponent(new GhostSimulator(currentShip, gameInputsListener));
        RenderableGroup ghostShapes = ghost.getComponent(RenderableGroup.class);
        ((SolidShape) ghostShapes.getShapeRenderer("Hull")).setFillColor(null);
        scene.addGameObject(ghost);
        ghost.activate();

        RulerDimensioner dimensioner = scene.getGameObjects().findGameObject(GameScene.RULER_GAME_OBJECT_NAME).getComponent(RulerDimensioner.class);
        dimensioner.autoUpdate(false);
        Locator worldLocation = scene.getCamera().getScreenProjection().inverseTransform(gameInputsListener.getMouseLocation());
        dimensioner.setTo(worldLocation);

        resetRoute();

        updateRuler();
    }

    @Override
    public void update(long dt) {
        super.update(dt);

        ShipModel currentShipModel = currentShip.getComponent(ModelContainer.class).getModel(ShipModel.class);

        GhostSimulator ghostSimulator = ghost.getComponent(GhostSimulator.class);
        if (portsideAction.isPressed() && ghostSimulator.getRoute().isEmpty()) {
            setBoard(ShipSide.PORTSIDE);
            resetRoute();
            updateRuler();
        } else if (starboardAction.isPressed() && ghostSimulator.getRoute().isEmpty()) {
            setBoard(ShipSide.STARBOARD);
            resetRoute();
            updateRuler();
        }
        // Si on clique pour valider un cap et qu'il y a encore des mouvements
        else if (storeCourseAction.isPressed() && currentShipModel.getMovementPool().hasMovement()) {
            storeCourse(currentShipModel, ghostSimulator);
        } else if (cancelCourseAction.isPressed() && !ghostSimulator.getRoute().isEmpty()) {
            cancelLastCourse(currentShipModel, ghostSimulator);
        } else if (validateCoursesAction.isPressed()) {
            validateCourses(ghostSimulator);
        }
    }

    private void validateCourses(GhostSimulator ghostSimulator) {
        Pilot currentPilot = currentShip.getComponent(Pilot.class);
        currentPilot.followRoute(ghostSimulator.getRoute());
    }

    private void cancelLastCourse(ShipModel currentShipModel, GhostSimulator ghostSimulator) {
        if (ghostSimulator.getRoute().removeLastCourse()) {
            currentShipModel.getMovementPool().cancel();
        }

        updateRuler();
    }

    private void storeCourse(ShipModel currentShipModel, GhostSimulator ghostSimulator) {
        // On récupère le point dans le monde
        Camera camera = scene.getCamera();
        Locator point = camera.getScreenProjection().inverseTransform(gameInputsListener.getMouseLocation());

        MovementPool movementPool = currentShipModel.getMovementPool();

        // On applique les contraintes de cap au point
        point = angleConstraints.apply(ghostSimulator.getRoute().getCurrentCourse(), point);
        // On applique les contraintes de longeur au point
        Locator course = Ruler.capToRange(ghostSimulator.getRoute().getCurrentCourse(), point, movementPool.greaterMovement());
        // TODO quand on clique pour valider une route mais que le point est plus loin que la colision, la route apparait jusqu'au point cliquer plutot que jusqu'à la proue du fantome
        // TODO quand on démarre une copie, on redémarre les copies des composants: est-ce que c'est nécessaire?
        // TODO il manque la gestion de la collision ici! Le ghost s'arrête à la collision, mais quand on ajoute le cap, on refait le calcul sans tenir compte d'un potentiel obstacle sur la route
        //  Il faudrait peut être voir à récupérer le point de la dernière simulation du fantôme? Ca éviterait de refaire les calculs
        // TODO autre bug surement lié: Quand on entre en collision avec une île et qu'on clique, le ghost bouge jusqu'au point cliqué et ne respecte pas la collision
        // TODO Si on entre collision avec une île "horizontale" par la gauche la rotation se bloque tant qu'on maintient la colision. La rotation est différente si on entre en collision par la droite.
        //  Etant donné que cette rotation découle de la simulation, pourquoi est-elle différente?

        // On consume le mouvement correspondant au cap
        double length = Math.round(ghostSimulator.getRoute().getCurrentCourse().distance(course));
        if (length > Ruler.getRangeLength(Range.S)) movementPool.consume(Range.L);
        else movementPool.consume(Range.S);

        // On ajoute le cap à la route
        ghostSimulator.getRoute().addCourse(course);

        // On met à jour la règle
        updateRuler();
    }

    @Override
    public Class<? extends GameMode> handleModeChange() {
        Pilot currentPilot = currentShip.getComponent(Pilot.class);

        if (currentPilot.hasCourses()) {
            ghost.destroy();
            ((PSMGameObject) scene.getGameObjects().findGameObject(GameScene.RULER_GAME_OBJECT_NAME)).disable();
            return MovingMode.class;
        }

        if (cancelModeAction.isPressed()) {
            ghost.destroy();
            ((PSMGameObject) scene.getGameObjects().findGameObject(GameScene.RULER_GAME_OBJECT_NAME)).disable();
            return NormalMode.class;
        }

        return null;
    }

    // TODO bon maintenant y'a peut être moyen de faire le tri dans ce bazar et virer le _updateRuler dès l'entré si nécessaire.
    private void updateRuler() {
        // On récupère le pool de mouvement
        ShipModel currentShipModel = currentShip.getComponent(ModelContainer.class).getModel(ShipModel.class);
        MovementPool movementPool = currentShipModel.getMovementPool();

        PSMGameObject ruler = (PSMGameObject) scene.getGameObjects().findGameObject(GameScene.RULER_GAME_OBJECT_NAME);
        if (movementPool.hasMovement()) {
            GhostSimulator ghostSimulator = ghost.getComponent(GhostSimulator.class);
            Locator from = ghostSimulator.getRoute().getCurrentCourse();
            ruler.getComponent(Transform.class).setToLocation(from);

            RulerDimensioner dimensioner = ruler.getComponent(RulerDimensioner.class);
            dimensioner.setRange(movementPool.greaterMovement());

            // TODO c'est pas optimum quand même a ce niveau. Certe on ne calcule q'une fois la manoeuvre, mais on constate que le ghost pourrait obetenir directement l'information, plutot qu'attendre d'être fournit
            angleConstraints = currentShip.getComponent(Pilot.class).getManeuverRange(ghostSimulator.getRoute());
            dimensioner.setManeuverRange(angleConstraints);
            ghostSimulator.setManeuverRange(angleConstraints);

            ruler.activate();
        } else {
            ruler.disable();
        }
    }

    private void setBoard(ShipSide side) {
        currentShip.getComponent(Pilot.class).setCurrentBoard(side);
        ghost.getComponent(Pilot.class).setCurrentBoard(side);
    }

    private void resetRoute() {
        Transform currentTransformation = currentShip.getComponent(Transform.class);
        Pilot pilot = currentShip.getComponent(Pilot.class);

        Locator proue = pilot.getCurrentBoard().getProue();
        proue = currentTransformation.apply(proue);

        // TODO La méthode resetTo est-elle nécessaire vu qu'on utilise la route dans le ghost desormais? et qu'on peut faire directement setRoute(new Route(proue))
        GhostSimulator ghostSimulator = ghost.getComponent(GhostSimulator.class);
        ghostSimulator.getRoute().resetTo(proue);
    }
}
