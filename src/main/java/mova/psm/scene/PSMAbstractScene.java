package mova.psm.scene;

import mova.game.core.AbstractScene;
import mova.game.core.ecs.container.GameObjectContainer;
import mova.game.graphics.MovaGraphics;
import mova.game.graphics.component.GameWindow;

public abstract class PSMAbstractScene extends AbstractScene {

    protected final GameWindow gameWindow;

    protected PSMAbstractScene(GameWindow gameWindow) {
        this.gameWindow = gameWindow;
    }

    protected PSMAbstractScene(GameObjectContainer gameObjectContainer, GameWindow gameWindow) {
        super(gameObjectContainer);

        this.gameWindow = gameWindow;
    }

    @Override
    public void draw(MovaGraphics graphics) {
        // TODO on peut envisager une option qui permette de clear l'écran à la demande (par Scene? par Jeu?)
        graphics.clearRect(0, 0, gameWindow.getWidth(), gameWindow.getHeight());

        super.draw(graphics);

        // TODO idem cette partie pourrait être optionnelle puisqu'elle découle de l'utilisation de SWING.
        gameWindow.paintContent(graphics);
    }
}
