package mova.psm.asset;

import mova.game.asset.ResourceCache;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.net.URL;

@Component
public final class ImageIconCache extends ResourceCache<ImageIcon> {

    @Override
    protected String getResourceRoot() {
        return "/images/";
    }

    @Override
    protected ImageIcon extract(URL url) {
        return new ImageIcon(url);
    }
}
