package mova.psm.debug;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.databind.ObjectMapper;
import mova.game.graphics.component.GameWindow;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RectangularShape;

@Component
final class CustomErrorMapperConfigurator implements ErrorMapperConfigurator {

    @JsonIgnoreType
    private interface IgnoreTypeMixIn {}

    private interface ShapeMixIn {
        @JsonIgnore
        Rectangle2D getBounds2D();
        @JsonIgnore
        Rectangle2D getBounds();
    }

    @Override
    public void configure(ObjectMapper mapper) {
        // TODO un serializer avec le nom de la font et sa taille
        mapper.addMixIn(Font.class, IgnoreTypeMixIn.class);
        mapper.addMixIn(Image.class, IgnoreTypeMixIn.class);
        mapper.addMixIn(GameWindow.class, IgnoreTypeMixIn.class);

        mapper.addMixIn(RectangularShape.class, ShapeMixIn.class);
    }
}
