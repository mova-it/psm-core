package mova.psm.asset;

import mova.game.asset.ResourceCache;
import mova.game.asset.wavefront.ObjectLoader;
import mova.game.graphics3D.PolygonGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.URL;

@Component
public final class PolygonGroupCache extends ResourceCache<PolygonGroup> {

    private final ObjectLoader objectLoader;

    @Autowired
    public PolygonGroupCache(ObjectLoader objectLoader) {
        this.objectLoader = objectLoader;
    }

    @Override
    protected String getResourceRoot() {
        return "/object/";
    }

    @Override
    protected PolygonGroup extract(URL url) {
        return objectLoader.loadObject(url);
    }
}
