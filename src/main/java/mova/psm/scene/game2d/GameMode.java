package mova.psm.scene.game2d;

interface GameMode {

    default void entry() {}

    default void update(long dt) {}

    default Class<? extends GameMode> handleModeChange() {
        return null;
    }

    default void release() {}

}
