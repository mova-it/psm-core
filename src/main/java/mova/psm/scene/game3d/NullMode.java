package mova.psm.scene.game3d;

import mova.game.core.ecs.GameObject;
import mova.game.graphics.MovaGraphics;
import mova.game.input.GameActionMapping;

import java.util.List;

final class NullMode implements GameMode {

    private final GameActionMapping gameActionMapping = new GameActionMapping();

    @Override
    public GameActionMapping getGameActionMapping() {
        return gameActionMapping;
    }

    @Override
    public void update(long dt, List<GameObject> gameObjects) {
        // Do Nothing
    }

    @Override
    public void draw(MovaGraphics graphics, List<GameObject> gameObjects) {
        // Do Nothing
    }
}
