package mova.psm.scene.game2d;

import mova.game.core.ecs.GameObject;
import mova.game.geom.Locator;
import mova.game.graphics.Transform;
import mova.game.graphics.component.GameWindow;
import mova.game.graphics.component.UserInterface;
import mova.game.input.GameAction;
import mova.game.input.GameInputsListener;
import mova.game.input.MouseUserAction;
import mova.game.model.ModelContainer;
import mova.game.physics.RigidBody;
import mova.game.view.Camera;
import mova.psm.admiral.Admiral;
import mova.psm.ecs.PSMGameObject;
import mova.psm.ship.ShipModel;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.geom.Rectangle2D;
import java.util.List;
import java.util.Optional;

final class NormalMode extends BaseGameMode {

    private final GameAction selectObjectAction;
    private final GameAction movePlanningAction;
    private final GameAction battlePlanningAction;
    private final GameAction switchPlayerAction;

    NormalMode(GameScene scene, GameWindow gameWindow, GameInputsListener gameInputsListener) {
        super(scene, gameWindow, gameInputsListener);

        selectObjectAction = gameActionMapping.mapToMouse("Select Ship", MouseUserAction.LEFT_BUTTON);

        UserInterface ui = scene.getUserInterface();
        movePlanningAction = ui.get("Move").getGameAction();
        battlePlanningAction = ui.get("Battle").getGameAction();
        switchPlayerAction = ui.get("Switch").getGameAction();

        gameActionMapping.mapToKey(movePlanningAction, KeyEvent.VK_M);
        gameActionMapping.mapToKey(battlePlanningAction, KeyEvent.VK_B);
        gameActionMapping.mapToKey(switchPlayerAction, KeyEvent.VK_P);
    }

    @Override
    public void entry() {
        super.entry();

        scene.updateUI();

        PSMGameObject marker = (PSMGameObject) scene.getGameObjects().findGameObject(GameScene.MARKER_GAME_OBJECT_NAME);
        MarkerComponent markerComponent = marker.getComponent(MarkerComponent.class);
        markerComponent.moveTo(scene.getSelectedGameObject());
        marker.show();
    }

    @Override
    public void update(long dt) {
        super.update(dt);

        if (selectObjectAction.isPressed()) {
            Locator mouseLocation = gameInputsListener.getMouseLocation();
            Locator mouseWorldLocation = scene.getCamera().getScreenProjection().inverseTransform(mouseLocation);

            GameObject gameObject = scene.getGameObjects().findGameObject(mouseWorldLocation);
            scene.selectGameObject((PSMGameObject) gameObject);
        }

        if (switchPlayerAction.isPressed()) {
            scene.nextAdmiral();

            List<PSMGameObject> ships = scene.getShips(scene.getCurrentAdmiral());
            Optional<PSMGameObject> optionalShip = ships.stream().filter(ship -> !ship.isDestroyed()).findAny();
            if (optionalShip.isPresent()) {
                // On récupère les coordonnées du bateau
                PSMGameObject ship = optionalShip.get();
                Shape shipShape = ship.getComponent(RigidBody.class).getBodyShape();
                shipShape = ship.getComponent(Transform.class).apply(shipShape);
                Rectangle2D shipShapeBounds = shipShape.getBounds2D();
                float sx = (float) shipShapeBounds.getCenterX();
                float sy = (float) shipShapeBounds.getCenterY();

                // On déplace la caméra
                Camera camera = scene.getCamera();
                camera.setX(sx);
                camera.setY(sy);

                // On set le nouveau bateau courant
                scene.selectGameObject(ship);

                PSMGameObject marker = (PSMGameObject) scene.getGameObjects().findGameObject(GameScene.MARKER_GAME_OBJECT_NAME);
                MarkerComponent markerComponent = marker.getComponent(MarkerComponent.class);
                markerComponent.moveTo(ship);
                marker.show();
            }
        }
    }

    @Override
    public Class<? extends GameMode> handleModeChange() {
        // TODO pour l'instant on controle qu'il y ait bien un bateau sélectionné, mais on ne devrait pas pouvoir faire l'action du tout (plutot que de l'empêcher de se réaliser)
        if (isShipSelected()) {
            if (movePlanningAction.isPressed()) return MovePlaningMode.class;
            if (battlePlanningAction.isPressed()) return BattlePlaningMode.class;
        }

        for (Admiral admiral : scene.admirals) {
            if (scene.getShips(admiral).isEmpty()) return WinMode.class;
        }

        return null;
    }

    private boolean isShipSelected() {
        GameObject currentGameObject = scene.getSelectedGameObject();
        if (currentGameObject == null) return false;

        ModelContainer modelContainer = currentGameObject.getComponent(ModelContainer.class);
        return modelContainer != null && modelContainer.hasModel(ShipModel.class);
    }
}
