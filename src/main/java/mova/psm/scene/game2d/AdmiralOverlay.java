package mova.psm.scene.game2d;

import mova.game.core.ecs.AbstractGameComponent;
import mova.game.graphics.MovaGraphics;
import mova.game.graphics.Transform;
import mova.psm.admiral.Admiral;
import mova.psm.asset.ImageCache;

import java.awt.*;
import java.awt.geom.Path2D;
import java.awt.image.BufferedImage;

public class AdmiralOverlay extends AbstractGameComponent {

    private final ImageCache imageCache;
    private Admiral admiral = null;
    private Path2D clip;
    private final Rectangle bounds;

    public AdmiralOverlay(ImageCache imageCache, Rectangle bounds) {
        this.imageCache = imageCache;
        this.bounds = bounds;
    }

    @Override
    public void start() {
        clip = new Path2D.Float();
        clip.moveTo(0.0, 0.0);
        clip.lineTo(100.0, 0.0);
        clip.lineTo(200.0, 100.0);
        clip.lineTo(200.0, 200.0);
        clip.lineTo(0.0, 200.0);
        clip.closePath();
    }

    @Override
    public void update(long dt) {
        admiral = ((GameScene) getGameObject().getScene()).getCurrentAdmiral();
    }

    @Override
    public void draw(MovaGraphics movaGraphics) {

        if (admiral != null) {
            Shape transformedClipShape = getGameObject().getComponent(Transform.class).apply(clip);
            movaGraphics.clip(transformedClipShape);

            Rectangle transformedBounds = getGameObject().getComponent(Transform.class).apply(bounds).getBounds();
            BufferedImage admiralImage = imageCache.getResource(admiral.getName().toLowerCase().contains("mohicane") ? "Aldo.jpg" : "Sylvane.jpg");
            movaGraphics.drawImage(admiralImage, transformedBounds.getLocation(), transformedBounds.getSize());

            movaGraphics.resetClip();
        }
    }

}
