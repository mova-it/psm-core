package mova.psm.debug;


import com.fasterxml.jackson.databind.ObjectMapper;

public interface ErrorMapperConfigurator {

    void configure(ObjectMapper mapper);
}
