package mova.psm.scene.game2d;

import mova.Settings;
import mova.game.core.ecs.GameObject;
import mova.game.core.ecs.container.StatefulGameObjectContainer;
import mova.game.event.EventSystem;
import mova.game.event.EventType;
import mova.game.geom.Locator;
import mova.game.geom.Segment;
import mova.game.graphics.MovaGraphics;
import mova.game.graphics.RenderableGroup;
import mova.game.graphics.SolidShape;
import mova.game.graphics.Transform;
import mova.game.graphics.component.*;
import mova.game.input.GameInputsListener;
import mova.game.model.ModelContainer;
import mova.game.view.Camera;
import mova.game.view.ConstrainedCamera;
import mova.game.view.View;
import mova.psm.admiral.Admiral;
import mova.psm.asset.ImageCache;
import mova.psm.asset.ImageIconCache;
import mova.psm.core.ship.NavalBattery;
import mova.psm.ecs.PSMGameObject;
import mova.psm.measure.RulerDimensioner;
import mova.psm.scene.PSMAbstractScene;
import mova.psm.scene.SceneUtils;
import mova.psm.ship.ShipModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Path2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO il faudrait voir a supprimer un maximum de RuntimeException dans le jeu puisqu'elles amènerait à crashé le jeu.
//  en sus, il faut soit envoyé une erreur pour les cas ou le jeu n'est pas stable ou gérer le cas avec un log warn ou error
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public final class GameScene extends PSMAbstractScene {

    public static final String BATTLE_BUTTON_NAME = "Battle";
    public static final String RULER_GAME_OBJECT_NAME = "ruler";
    public static final String MARKER_GAME_OBJECT_NAME = "marker";
    public static final String CANNONEER_GAME_OBJECT_NAME = "cannoneer";
    private final ImageCache imageCache;
    private final ImageIconCache imageIconCache;
    private final Shipyard shipyard;
    private final MapBuilder mapBuilder;
    private final GameInputsListener gameInputsListener;

    private View view;


    private Map<Class<? extends GameMode>, GameMode> modes;

    private Class<? extends GameMode> currentModeClass;

    private PSMGameObject selectedGameObject = null;

    public final List<Admiral> admirals = new ArrayList<>();
    private final Map<Admiral, List<PSMGameObject>> admiralsShips = new HashMap<>();
    private int currentAdmiralIndex;

    private final Map<GameObject, Long> sunkenShips = new HashMap<>();


    @Autowired
    GameScene(GameWindow gameWindow, ImageCache imageCache, ImageIconCache imageIconCache, Shipyard shipyard, MapBuilder mapBuilder, GameInputsListener gameInputsListener) {
        super(new StatefulGameObjectContainer(), gameWindow);

        this.imageCache = imageCache;
        this.imageIconCache = imageIconCache;
        this.shipyard = shipyard;
        this.mapBuilder = mapBuilder;
        this.gameInputsListener = gameInputsListener;
    }

    @Override
    public void start() {
        EventSystem.notify(EventType.GAME_START);

        view = new View(0, 0, gameWindow.getWidth(), gameWindow.getHeight() - 200);

        ConstrainedCamera camera = view.addCamera(ConstrainedCamera.class);
        camera.stickToWorld(GameContextHolder.getGameContext().getMap());
        camera.adaptMinScaleFactorToBoundsConstraints();
        camera.setMaxScaleFactor(1);
        camera.setScaleFactorLevel(.33f);

        GameButton quitButton = createGameButton("Quit", "menu/quit.png");
        GameButton moveButton = createGameButton("Move", "menu/wheel.png");
        GameButton shootButton = createGameButton(BATTLE_BUTTON_NAME, "menu/cannon.png");
        GameButton switchButton = createGameButton("Switch", "menu/play.png");

        GamePanel buttons = new GamePanel(new FlowLayout());
        buttons.add(quitButton);
        buttons.add(moveButton);
        buttons.add(shootButton);
        buttons.add(switchButton);

        gameWindow.addContents(buttons);


        gameInputsListener.listenTo(gameWindow);


        // TODO peut-on récupérer via Spring les mode? oui, mais uniquement si la Scene est fournit autrement que via l'injection à cause des cycles
        modes = new HashMap<>();
        modes.put(NullMode.class, new NullMode());
        modes.put(NormalMode.class, new NormalMode(this, gameWindow, gameInputsListener));
        modes.put(MovePlaningMode.class, new MovePlaningMode(this, gameWindow, gameInputsListener));
        modes.put(MovingMode.class, new MovingMode(this, gameWindow, gameInputsListener));
        modes.put(BattlePlaningMode.class, new BattlePlaningMode(this, gameWindow, gameInputsListener));
        modes.put(BattlingMode.class, new BattlingMode(this, gameWindow, gameInputsListener));
        modes.put(WinMode.class, new WinMode(this, gameWindow, gameInputsListener));
        modes.put(ConfigurationMode.class, new ConfigurationMode(this, gameWindow, gameInputsListener));

        currentModeClass = NullMode.class;
        setMode(ConfigurationMode.class);




        GameContext gameContext = GameContextHolder.getGameContext();

        PSMGameObject seaMap = mapBuilder.createMap(gameContext.getMap());
        addGameObject(seaMap);
        seaMap.show();

        List<PSMGameObject> islands = mapBuilder.createIslands(gameContext.getIslands());
        addGameObjects(islands);

        for (int nameIndex = 0; nameIndex < gameContext.getAdmiralsNames().size(); nameIndex++) {
            String admiralName = gameContext.getAdmiralsNames().get(nameIndex);
            Admiral admiral = () -> admiralName;

            admirals.add(admiral);

            List<String> admiralShipReferences = gameContext.getAdmiralsShips().get(admiral.getName());
            List<PSMGameObject> admiralShips = new ArrayList<>();
            for (String shipReference : admiralShipReferences) {
                PSMGameObject ship = shipyard.createShip(shipReference);
                addGameObject(ship);

                ship.getComponent(ModelContainer.class).getModel(ShipModel.class).takeOrdersFrom(admiral);
                admiralShips.add(ship);
            }

            admiralsShips.put(admiral, admiralShips);
        }

        createMarker();
        createRuler();
        createCannoneer();
        createOverlays();


        currentAdmiralIndex = 0;

        super.start();
    }

    @Override
    public void release() {
        // TODO il faut revoir complètement le release de chaque mode
        super.release();

        gameInputsListener.detach();

        gameWindow.clearContent();

        modes.clear();
        modes = null;
    }

    @Override
    public void update(long dt) {
        GameMode mode = modes.get(currentModeClass);

        // On commence par vérifier les controls. On est encore attaché à un mode
        mode.update(dt);

        super.update(dt);

        cleanSunkenShips(dt);

        // On vérifie s'il y a un changement de mode et si oui on le change
        Class<? extends GameMode> newModeClass = mode.handleModeChange();
        if (newModeClass != null) setMode(newModeClass);

        if (getUserInterface().get("Quit").getGameAction().isPressed()) {
            EventSystem.notify(EventType.GAME_END);
        }
    }

    @Override
    public void draw(MovaGraphics graphics) {
        graphics.clearRect(0, 0, gameWindow.getWidth(), gameWindow.getHeight());

        super.draw(graphics);

        gameWindow.paintContent(graphics);

        if (HomeIslandSelectionPhase.locators != null) {
            for (Locator locator : HomeIslandSelectionPhase.locators) {
                if (locator != null) {
                    Locator projectedLocator = getCamera().getScreenProjection().transform(locator);
                    graphics.setColor(Color.BLACK);
                    graphics.fillRoundRect((int) projectedLocator.getX(), (int) projectedLocator.getY(), 10, 10, 1, 1);
                }
            }
        }

    }

    void updateUI() {
        // TODO C'est pas le truc le plus fou que j'ai fait ca non plus... Est-ce que l'UI pourrait pas être au final un GameObject aussi? Ou recevoir/envoyer des évènements!!!
        if (selectedGameObject != null) {
            if (selectedGameObject.getName().startsWith("ship")) {
                Admiral currentAdmiral = getCurrentAdmiral();
                ShipModel shipModel = selectedGameObject.getComponent(ModelContainer.class).getModel(ShipModel.class);
                boolean playable = !selectedGameObject.is("played") && shipModel.getAdmiral().getName().equals(currentAdmiral.getName());
                gameWindow.getUserInterface().get("Move").setEnabled(playable);
                gameWindow.getUserInterface().get(BATTLE_BUTTON_NAME).setEnabled(playable);
            } else {
                gameWindow.getUserInterface().get("Move").setEnabled(false);
                gameWindow.getUserInterface().get(BATTLE_BUTTON_NAME).setEnabled(false);
            }

            // TODO Quand on active le MovingMode on ne veut pas voir le marker mais on met quand même a jour l'UI
            //  Si on active le marker à chaque fois que l'on update l'UI, le marker redevient visible après qu'on l'ait caché
            //  Faut vraiment refaire cette partie avant que ca devienne ingérable
            if (getGameObjects().findGameObject(MARKER_GAME_OBJECT_NAME).isVisible()) {
                MarkerComponent markerComponent = getGameObjects().findGameObject(MARKER_GAME_OBJECT_NAME).getComponent(MarkerComponent.class);
                markerComponent.moveTo(getSelectedGameObject());
            }
        }
    }

    List<PSMGameObject> getShips(Admiral admiral) {
        return new ArrayList<>(admiralsShips.get(admiral));
    }

    // TODO la méthode est appelé par les modes qui présument potentiellement à tord qu'il s'agit d'un bateau et que cette valeur est non null
    PSMGameObject getSelectedGameObject() {
        return selectedGameObject;
    }

    void selectGameObject(PSMGameObject gameObject) {
        selectedGameObject = gameObject;

        updateUI();
    }

    Admiral getCurrentAdmiral() {
        return admirals.get(currentAdmiralIndex);
    }

    void nextAdmiral() {
        setAdmiral((currentAdmiralIndex + 1) % admirals.size());
    }

    void setAdmiral(int index) {
        currentAdmiralIndex = index;

        updateUI();
    }

    @Override
    public Camera getCamera() {
        return view.getActiveCamera();
    }

    @Override
    public View getView() {
        return view;
    }

    UserInterface getUserInterface() {
        return gameWindow.getUserInterface();
    }

    private void cleanSunkenShips(long dt) {
        getGameObjects().forEach(gameObject -> {
            ModelContainer modelContainer = gameObject.getComponent(ModelContainer.class);
            if (modelContainer != null) {
                ShipModel shipModel = modelContainer.getModel(ShipModel.class);
                if (shipModel != null && shipModel.isSunken()) sunkenShips.putIfAbsent(gameObject, dt);
            }
        });

        for (GameObject gameObject : sunkenShips.keySet()) {
            long sunkenTime = sunkenShips.compute(gameObject, (gameObject1, aLong) -> aLong != null ? aLong + dt : dt);
            if (sunkenTime >= 2000) {
                for (Admiral admiral : admirals) {
                    //noinspection SuspiciousMethodCalls
                    admiralsShips.get(admiral).remove(gameObject);
                }
                ((PSMGameObject) gameObject).destroy();
            }
        }
    }

    private void setMode(Class<? extends GameMode> modeClass) {
        // TODO réfléchir à l'idée d'utiliser une enum contenant la class des modes plutot que la classe directement. Quels intérêts?
        if (!modeClass.equals(currentModeClass)) {
            GameMode oldMode = modes.get(currentModeClass);
            oldMode.release();

            currentModeClass = modeClass;

            GameMode newMode = modes.get(currentModeClass);
            newMode.entry();
        }
    }

    private GameButton createGameButton(String name, String imagePath) {
        ImageIcon icon = imageIconCache.getResource(imagePath);
        IconSet iconSet = SceneUtils.createIconSet(gameWindow, icon);
        GameButton button = SceneUtils.createIconButton(name, iconSet);
        button.addActionListener(gameInputsListener);

        return button;
    }

    private void createMarker() {
        PSMGameObject marker = new PSMGameObject(MARKER_GAME_OBJECT_NAME);

        Shape circle = new Ellipse2D.Float(0, 0, 0, 0);
        marker.addComponent(new SolidShape(circle, new Color(0, 0, 0, .5f), Settings.DEFAULT_STROKE, Color.WHITE));
        marker.addComponent(new Transform());
        marker.addComponent(new MarkerComponent());

        addGameObject(marker);
    }

    private void createRuler() {
        PSMGameObject ruler = new PSMGameObject(RULER_GAME_OBJECT_NAME);

        ruler.addComponent(new Transform()).setZIndex(3);
        ruler.addComponent(new RulerDimensioner(gameInputsListener));

        RenderableGroup shapes = ruler.addComponent(new RenderableGroup());
        shapes.addShapeRenderer("L", new SolidShape(new Segment(), Settings.DEFAULT_STROKE, Color.RED));
        shapes.addShapeRenderer("S", new SolidShape(new Segment(), Settings.DEFAULT_STROKE, Color.WHITE));
        shapes.getShapeRenderers().forEach(shapeRenderer -> shapeRenderer.setMutable(true));

        addGameObject(ruler);
    }

    private void createCannoneer() {
        PSMGameObject cannoneer = new PSMGameObject(CANNONEER_GAME_OBJECT_NAME);

        cannoneer.addComponent(new Transform()).setZIndex(3);
        cannoneer.addComponent(new NavalBattery());

        addGameObject(cannoneer);
    }

    private void createOverlays() {
        PSMGameObject descGO = new PSMGameObject("Description Overlay");

        Rectangle descriptionBounds = new Rectangle(0, 0, 600, 200);
        descGO.addComponent(new Transform(AffineTransform.getTranslateInstance((view.getWidth() - 600) / 2f, view.getHeight()), Integer.MAX_VALUE));
        descGO.addComponent(new SolidShape(descriptionBounds, new Color(7219479), new BasicStroke(10.0F), Color.BLACK));
        descGO.addComponent(new DescriptionOverlay(descriptionBounds));

        descGO.applyIfPresent(SolidShape.class, solidShape -> {
            solidShape.setProjected(false);
            solidShape.forceViewability(true);
        });

        addGameObject(descGO);
        descGO.activate();


        Rectangle admiralBounds = new Rectangle(0, 0, 200, 200);

        Path2D constructedBorder = new Path2D.Float();
        constructedBorder.moveTo(0.0, 0.0);
        constructedBorder.lineTo(100.0, 0.0);
        constructedBorder.lineTo(200.0, 100.0);
        constructedBorder.lineTo(200.0, 200.0);

        PSMGameObject admiralGO = new PSMGameObject("Admiral Overlay");
        admiralGO.addComponent(new Transform(AffineTransform.getTranslateInstance((view.getWidth() - 600) / 2f + 600, view.getHeight()), Integer.MAX_VALUE));
        admiralGO.addComponent(new RenderableGroup());
        admiralGO.addComponent(new AdmiralOverlay(imageCache, admiralBounds));

        admiralGO.applyIfPresent(RenderableGroup.class, renderableGroup -> {
            SolidShape outsideBorder = renderableGroup.addShapeRenderer("Outside border", new SolidShape(constructedBorder, new BasicStroke(10.0F), new Color(7219479)));
            SolidShape insideBorder = renderableGroup.addShapeRenderer("Inside border", new SolidShape(constructedBorder, new BasicStroke(5.0F), Color.BLACK));

            outsideBorder.setProjected(false);
            outsideBorder.forceViewability(true);
            insideBorder.setProjected(false);
            insideBorder.forceViewability(true);
        });

        addGameObject(admiralGO);
        admiralGO.activate();

        PSMGameObject backOverlayGO = new PSMGameObject("Overlay background");
        backOverlayGO.addComponent(new Transform(AffineTransform.getTranslateInstance(0, view.getHeight()), Integer.MAX_VALUE - 1));
        backOverlayGO.addComponent(new SolidShape(new Rectangle(0, 0, gameWindow.getWidth(), 200), new Color(7219479), new BasicStroke(10.0F), Color.BLACK));

        backOverlayGO.applyIfPresent(SolidShape.class, solidShape -> {
            solidShape.setProjected(false);
            solidShape.forceViewability(true);
        });

        addGameObject(backOverlayGO);
        backOverlayGO.show();
    }

}