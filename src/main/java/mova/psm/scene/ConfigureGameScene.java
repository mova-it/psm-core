package mova.psm.scene;

import mova.game.core.SceneOrchestrator;
import mova.game.dice.Die;
import mova.game.graphics.ImageUtils;
import mova.game.graphics.MovaGraphics;
import mova.game.graphics.UpdatableGif;
import mova.game.graphics.component.GameButton;
import mova.game.graphics.component.GamePanel;
import mova.game.graphics.component.GameWindow;
import mova.game.graphics.component.UserInterface;
import mova.game.graphics.component.event.GameActionListener;
import mova.psm.asset.FontCache;
import mova.psm.asset.GifCache;
import mova.psm.map.LandRegister;
import mova.psm.scene.game2d.GameContext;
import mova.psm.scene.game2d.GameContextHolder;
import mova.psm.scene.game2d.GameScene;
import mova.psm.scene.game3d.GameScene3D;
import mova.psm.ship.model.ShipInfoRegister;
import mova.util.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.awt.font.TextAttribute;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public final class ConfigureGameScene extends PSMAbstractScene implements GameActionListener {

    private static final GridBagConstraints MENU_LAYOUT_CONSTRAINTS = new GridBagConstraints();
    static {
        MENU_LAYOUT_CONSTRAINTS.gridwidth = GridBagConstraints.REMAINDER;
        MENU_LAYOUT_CONSTRAINTS.anchor = GridBagConstraints.CENTER;
        MENU_LAYOUT_CONSTRAINTS.fill = GridBagConstraints.HORIZONTAL;
        MENU_LAYOUT_CONSTRAINTS.insets = new Insets(10, 1000, 10, 0);
    }

    private final SceneOrchestrator sceneOrchestrator;
    private final GifCache gifCache;
    private final FontCache fontCache;
    private final ShipInfoRegister shipInfoRegister;
    private final LandRegister landRegister;

    private UpdatableGif homeBackground;
    private UpdatableGif pirateFlag;

    @Autowired
    public ConfigureGameScene(SceneOrchestrator sceneOrchestrator, GameWindow gameWindow, GifCache gifCache, FontCache fontCache, ShipInfoRegister shipInfoRegister, LandRegister landRegister) {
        super(gameWindow);

        this.sceneOrchestrator = sceneOrchestrator;
        this.gifCache = gifCache;
        this.fontCache = fontCache;
        this.shipInfoRegister = shipInfoRegister;
        this.landRegister = landRegister;
    }

    @Override
    public void start() {
        Font pirateFont = fontCache.getResource("WreckedShip_custom.ttf", Font.PLAIN, 60);
        Font underlinedPirateFont = fontCache.getResource(pirateFont, Collections.singletonMap(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON));

        UserInterface userInterface = gameWindow.getUserInterface();
        GameButton launchGameButton = UserInterface.createTextualButton("Lancer la partie", pirateFont, underlinedPirateFont);
        GameButton launchGame3DButton =  UserInterface.createTextualButton("Lancer la partie en 3D", pirateFont, underlinedPirateFont);
        GameButton customButton = UserInterface.createTextualButton("Personnaliser", pirateFont, underlinedPirateFont);
        GameButton backButton = UserInterface.createTextualButton("Retour", pirateFont, underlinedPirateFont);

        userInterface.add(launchGameButton);
        userInterface.add(launchGame3DButton);
        userInterface.add(customButton);
        userInterface.add(backButton);

        GamePanel buttonPanel = new GamePanel(new GridBagLayout());
        buttonPanel.add(launchGameButton, MENU_LAYOUT_CONSTRAINTS);
        buttonPanel.add(customButton, MENU_LAYOUT_CONSTRAINTS);
        buttonPanel.add(launchGame3DButton, MENU_LAYOUT_CONSTRAINTS);
        buttonPanel.add(backButton, MENU_LAYOUT_CONSTRAINTS);

        gameWindow.addContents(BorderLayout.CENTER, buttonPanel);

        launchGameButton.addActionListener(this);
        launchGame3DButton.addActionListener(this);
        customButton.addActionListener(this);
        backButton.addActionListener(this);

        if (Die.D2.roll() == 1) {
            homeBackground = gifCache.getResource("ezgif.com-gif-maker(1).gif");
        } else {
            homeBackground = gifCache.getResource("ezgif.com-gif-maker(3).gif");
        }

        if (Die.D2.roll() == 1) {
            pirateFlag = gifCache.getResource("pirate-flag-x.gif");
        } else {
            pirateFlag = gifCache.getResource("pirate-flag-16.gif");
        }

        super.start();
    }

    @Override
    public void release() {
        gameWindow.getUserInterface().clear();

        gameWindow.clearContent();

        homeBackground = null;
        pirateFlag = null;
    }

    @Override
    public void update(long dt) {
        homeBackground.update(dt);
        pirateFlag.update(dt);

        if (gameWindow.getUserInterface().get("Lancer la partie").isPressed()) {
            GameContext gameContext = createParty();
            GameContextHolder.setGameContext(gameContext);

            sceneOrchestrator.setScene(GameScene.class);
        }
        else if (gameWindow.getUserInterface().get("Personnaliser").isPressed()) {
            GameContext gameContext = createParty();
            gameContext.getAdmiralsNames().add("Gabriel Macerot");
            gameContext.getAdmiralsShips().put("Gabriel Macerot", gameContext.getAdmiralsShips().values().stream().findAny().orElseThrow(NullPointerException::new));
            GameContextHolder.setGameContext(gameContext);

            sceneOrchestrator.setScene(GameScene.class);
        }
        else if (gameWindow.getUserInterface().get("Lancer la partie en 3D").isPressed()) {
            sceneOrchestrator.setScene(GameScene3D.class);
        }
        else if (gameWindow.getUserInterface().get("Retour").isPressed()) {
            sceneOrchestrator.setScene(HomeScene.class);
        }
    }

    @Override
    public void draw(MovaGraphics graphics) {
        graphics.drawImage(homeBackground, gameWindow.getSize());

        BufferedImage flag = (BufferedImage) pirateFlag.getCurrentFrame();
        BufferedImage transparentFlagCache = ImageUtils.createTranslucent(gameWindow, flag, .1f);
        graphics.drawImage(transparentFlagCache, gameWindow.getSize());

        gameWindow.paintContent(graphics);
    }

    private GameContext createParty() {
        Rectangle map = new Rectangle(22500, 22500);

        List<String> admiralsNames = Arrays.asList("Zinzin Moretto", "Mohicane De Beauvent");
        List<String> shipReferences = shipInfoRegister.getReferences();

        Map<String, List<String>> admiralsShips = new HashMap<>();
        for (String admiralsName : admiralsNames) {
            List<String> admiralShipReferences = Stream.generate(() -> RandomUtils.randElement(shipReferences)).limit(3).collect(Collectors.toList());

            admiralsShips.put(admiralsName, admiralShipReferences);
        }

        Shape[] randomIslandShapes = new Shape[admiralsNames.size() * 3];
        for (int i = 0; i < randomIslandShapes.length; i++) {
            randomIslandShapes[i] = landRegister.getLandShape("island" + RandomUtils.randInt(7));
        }

        return new GameContext(map, Arrays.asList(randomIslandShapes), admiralsNames, admiralsShips);
    }
}
