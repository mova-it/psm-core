package mova.psm.asset;

import mova.game.asset.ResourceCache;
import mova.game.graphics.ImageUtils;
import org.springframework.stereotype.Component;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

@Component
public final class ImageCache extends ResourceCache<BufferedImage> {

    @Override
    protected String getResourceRoot() {
        return "/images/";
    }

    @Override
    protected BufferedImage extract(URL url) throws IOException {
        return ImageUtils.loadImage(url);
    }
}
