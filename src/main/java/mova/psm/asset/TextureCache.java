package mova.psm.asset;

import mova.game.asset.ResourceCache;
import mova.game.graphics3D.texture.Texture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.awt.image.BufferedImage;
import java.net.URL;

@Component
public final class TextureCache extends ResourceCache<Texture> {

    public static final String TEXTURE_KEY_SEPARATOR = "/";
    private final ImageCache imageCache;

    @Autowired
    public TextureCache(ImageCache imageCache) {
        this.imageCache = imageCache;
    }

    @Override
    protected String getResourceRoot() {
        return imageCache.getResourceRoot();
    }

    @Override
    protected Texture extract(URL url) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Texture getResource(String resourceName) {
        return getResource(resourceName, false);
    }

    // TODO est-ce que le textureCache à vraiment son utilité? Est ce que cela prend en compte l'orientation
    //      de la texture par exemple auquel cas ne faudra-t-il pas qu'elle soit dépendante du polygon?

    public Texture getResource(String resourceName, boolean shaded) {
        return resources.computeIfAbsent(resourceName + TEXTURE_KEY_SEPARATOR + shaded, name -> {
            BufferedImage image = imageCache.getResource(resourceName);
            return Texture.createTexture(image, shaded);
        });
    }
}
