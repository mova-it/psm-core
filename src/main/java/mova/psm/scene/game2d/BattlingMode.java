package mova.psm.scene.game2d;

import mova.game.graphics.component.GameWindow;
import mova.game.input.GameInputsListener;
import mova.psm.core.ship.NavalBattery;
import mova.psm.ecs.PSMGameObject;

final class BattlingMode extends BaseGameMode {

    BattlingMode(GameScene scene, GameWindow gameWindow, GameInputsListener gameInputsListener) {
        super(scene, gameWindow, gameInputsListener);
    }

    @Override
    public void entry() {
        super.entry();

        ((PSMGameObject) scene.getGameObjects().findGameObject(GameScene.MARKER_GAME_OBJECT_NAME)).disable();
        PSMGameObject cannoneer = (PSMGameObject) scene.getGameObjects().findGameObject(GameScene.CANNONEER_GAME_OBJECT_NAME);
        cannoneer.getComponent(NavalBattery.class).fire();
        cannoneer.disable();

        PSMGameObject currentShip = scene.getSelectedGameObject();
        currentShip.set("played", true);
        scene.updateUI();
    }

    @Override
    public Class<? extends GameMode> handleModeChange() {
        return NormalMode.class;
    }
}
