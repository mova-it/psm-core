package mova.psm.asset;

import java.awt.*;
import java.util.Objects;

class FontResource extends Font {

    private final String resourceName;

    FontResource(Font font, String resourceName) {
        super(font);

        this.resourceName = resourceName;
    }

    public String getResourceName() {
        return resourceName;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof FontResource)) return false;

        String otherResourceName = ((FontResource) obj).resourceName;
        return Objects.equals(resourceName, otherResourceName) && super.equals(obj);
    }

    @Override
    public int hashCode() {
        return Objects.hash(resourceName, super.hashCode());
    }
}
