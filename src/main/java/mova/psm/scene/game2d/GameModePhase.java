package mova.psm.scene.game2d;

import mova.game.core.FatalError;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class GameModePhase<M extends GameMode> {

    protected final M mode;

    protected GameModePhase(M mode) {
        this.mode = mode;
    }

    void entry() {}

    void update(long dt) {}

    void release() {}

    @SafeVarargs
    static <M extends GameMode> List<GameModePhase<M>> createPhasesSequence(M mode, Class<? extends GameModePhase<M>>... phases) {
        return Stream.of(phases).map(gameModePhaseClass -> {
            try {
                return gameModePhaseClass.getDeclaredConstructor(mode.getClass()).newInstance(mode);
            } catch (ReflectiveOperationException e) {
                throw new FatalError("Impossible d'instancier le GameModePhase: " + gameModePhaseClass, e);
            }
        }).collect(Collectors.toList());
    }
}
