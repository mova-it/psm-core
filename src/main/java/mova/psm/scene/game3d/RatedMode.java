package mova.psm.scene.game3d;

import mova.game.core.ecs.GameObject;
import mova.game.graphics.MovaGraphics;
import mova.game.graphics3D.PolygonRenderer;
import mova.game.graphics3D.math3D.Camera;
import mova.game.input.GameActionMapping;

import java.awt.*;
import java.util.List;

public class RatedMode extends NormalMode {



    // for calculating frame rate
    private int numFrames;
    private long startTime;
    private float frameRate;

    private long updateTime;


    RatedMode(GameActionMapping parentMapping, Camera camera, PolygonRenderer polygonRenderer) {
        super(parentMapping, camera, polygonRenderer);
    }

    @Override
    public void entry() {
        numFrames = 0;
        startTime = System.nanoTime();
    }

    @Override
    public void update(long dt, List<GameObject> gameObjects) {
        updateTime = System.nanoTime();

        super.update(dt, gameObjects);

        updateTime = System.nanoTime() - updateTime;
    }

    @Override
    public void draw(MovaGraphics graphics, List<GameObject> gameObjects) {

        long drawTime = System.nanoTime();

        super.draw(graphics, gameObjects);

        drawTime = System.nanoTime() - drawTime;

        graphics.setColor(Color.WHITE);
        calcFrameRate();
        graphics.drawString(frameRate + " frames/sec", 5, 25);
        graphics.drawString(updateTime + " ns updating", 5, 50);
        graphics.drawString(drawTime + " ns drawing", 5, 75);
    }

    public void calcFrameRate() {
        numFrames++;
        long currTime = System.nanoTime();

        // calculate the frame rate every 500 milliseconds
        if (currTime > startTime + 500000000) {
            frameRate = (float) numFrames * 1000000000 / (currTime - startTime);
            startTime = currTime;
            numFrames = 0;
        }
    }
}
