package mova.psm.asset;

import mova.game.asset.wavefront.ObjectLoader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AssetConfig {

    // TODO le package Asset commence à être fourni...
    @Bean
    public ObjectLoader objectLoader(ImageCache imageCache) {
        return new ObjectLoader(imageCache::getResource);
    }
}
