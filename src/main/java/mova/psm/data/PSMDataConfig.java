package mova.psm.data;

import mova.jpa.NaturalIdRepositoryImpl;
import mova.psm.PSMConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackageClasses = PSMConfig.class, repositoryBaseClass = NaturalIdRepositoryImpl.class)
public class PSMDataConfig {

}
