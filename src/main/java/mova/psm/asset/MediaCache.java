package mova.psm.asset;

import javafx.scene.media.Media;
import mova.game.asset.ResourceCache;
import mova.game.core.FatalError;
import org.springframework.stereotype.Component;

import java.net.URISyntaxException;
import java.net.URL;

@Component
public final class MediaCache extends ResourceCache<Media> {

    @Override
    protected String getResourceRoot() {
        return "/mp3/";
    }

    @Override
    protected Media extract(URL url) {
        try {
            return new Media(url.toURI().toString());
        } catch (URISyntaxException e) {
            throw new FatalError("Impossible de charger le media: " + url, e);
        }
    }
}
