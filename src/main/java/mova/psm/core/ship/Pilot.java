package mova.psm.core.ship;

import mova.game.core.Copyable;
import mova.game.core.ecs.AbstractGameComponent;
import mova.game.geom.Locator;
import mova.game.graphics.Transform;
import mova.game.model.ModelContainer;
import mova.psm.measure.AngleConstraints;
import mova.psm.measure.Route;
import mova.psm.ship.Board;
import mova.psm.ship.ShipSide;
import mova.psm.ship.ShipTemplate;
import mova.trigo.TrigoUtils;

import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.List;

public final class Pilot extends AbstractGameComponent implements Copyable<Pilot> {

    private static final float SHIP_SPEED = .2f;

    private Compass compass;

    private ShipTemplate shipTemplate;

    private ShipSide currentShipSide;

    private final List<Locator> courses = new ArrayList<>();

    public Pilot() {}

    private Pilot(Pilot pilot) {
        shipTemplate = pilot.shipTemplate;
        currentShipSide = pilot.currentShipSide;
        compass = pilot.compass;
    }

    @Override
    public void start() {
        shipTemplate = getGameObject().getComponent(ModelContainer.class).getModel(ShipTemplate.class);

        currentShipSide = ShipSide.PORTSIDE;

        compass = new Compass(SHIP_SPEED);
    }

    @Override
    public void update(long elapsedTime) {
        if (hasCourses()) {

            Transform transform = getGameObject().getComponent(Transform.class);

            Board board = getCurrentBoard();
            Locator proue = transform.apply(board.getProue());
            Locator poupe = transform.apply(board.getPoupe());
            Locator destination = getCourses().get(0);

            AffineTransform movement = compass.nextMovement(elapsedTime, proue, poupe, destination);
            transform.add(movement);

            proue = transform.apply(board.getProue());
            if (destination.equals(proue)) consumeCurrentCourse();
        }
    }

    public void toRouteEnd() {
        while (hasCourses()) {
            Transform transform = getGameObject().getComponent(Transform.class);

            Board board = getCurrentBoard();
            Locator proue = transform.apply(board.getProue());
            Locator poupe = transform.apply(board.getPoupe());
            Locator destination = getCourses().get(0);

            AffineTransform movement = compass.toDestination(proue, poupe, destination);
            transform.add(movement);

            consumeCurrentCourse();
        }
    }

    public void setCurrentBoard(ShipSide shipSide) {
        currentShipSide = shipSide;
    }

    public Board getCurrentBoard() {
        return shipTemplate.getBoards().get(currentShipSide);
    }

    public void stop() {
        courses.clear();
    }

    public void followRoute(Route route) {
        courses.clear();
        courses.addAll(route.getCourses());
    }

    public boolean hasCourses() {
        return !courses.isEmpty();
    }

    public List<Locator> getCourses() {
        return courses;
    }

    private void consumeCurrentCourse() {
        getCourses().remove(0);
        compass.notifyCourseChange();
    }

    @Override
    public Pilot copy() {
        return new Pilot(this);
    }

    public AngleConstraints getManeuverRange(Route route) {
        Board board = getCurrentBoard();

        double lastDirection;
        if (route.isEmpty()) {
            Transform currentTransformation = getGameObject().getComponent(Transform.class);
            board = (Board) currentTransformation.apply(board, board.copy());
            lastDirection = board.direction();
        } else lastDirection = route.getLastDirection();

        return Pilot.getManeuverRange(lastDirection, board.getSide());
    }

    // TODO cette méthode maintenant static concerne le calcul de la contrainte angulaire poour un navire ; à mon avis, il y en aura également pour les fort, les canons, etc.
    //  Il faudra donc lier ces méthodes au(x) bon(s) objet(s)
    public static AngleConstraints getManeuverRange(double lastDirection, ShipSide side) {
        double minPhi = TrigoUtils.normalize(lastDirection + side.getMinAngle());
        double maxPhi = TrigoUtils.normalize(lastDirection + side.getMaxAngle());

        return new AngleConstraints(minPhi, maxPhi);
    }
}
