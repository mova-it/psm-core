package mova.psm.graphics;

import mova.game.graphics.component.GameWindow;
import mova.game.graphics.Screen;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class PSMGraphicsConfig {

    @PostConstruct
    private void init() {
        LoggerFactory.getLogger(PSMGraphicsConfig.class).debug("Configuration graphique");
    }

    @Bean
    public Screen screen() {
        return new Screen();
    }

    @Bean
    public GameWindow gameWindow() {
        return new GameWindow();
    }
}
