package mova.psm.scene.game2d;

import mova.Settings;
import mova.game.geom.Segment;
import mova.game.graphics.RenderableGroup;
import mova.game.graphics.SolidShape;
import mova.game.graphics.Transform;
import mova.game.model.DescriptionProvider;
import mova.game.model.ModelContainer;
import mova.game.physics.RigidBody;
import mova.psm.core.ship.Pilot;
import mova.psm.ecs.PSMGameObject;
import mova.psm.ship.Board;
import mova.psm.ship.ShipModel;
import mova.psm.ship.ShipTemplate;
import mova.psm.ship.ShipTemplateRegister;
import mova.psm.ship.model.ShipInfo;
import mova.psm.ship.model.ShipInfoRegister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.awt.*;

@Component
final class Shipyard {

    // TODO pour les couleurs on pourrait créer une palette ou un ColorCache
    private static final Color DEFAULT_HULL_COLOR = new Color(0x663300);
    private static final Color DEFAULT_DECK_COLOR = new Color(0x774400);

    private final ShipTemplateRegister shipTemplateRegister;
    private final ShipInfoRegister shipInfoRegister;

    @Autowired
    Shipyard(ShipTemplateRegister shipTemplateRegister, ShipInfoRegister shipInfoRegister) {
        this.shipTemplateRegister = shipTemplateRegister;
        this.shipInfoRegister = shipInfoRegister;
    }

    PSMGameObject createShip(String ref) {
        ShipInfo shipInfo = shipInfoRegister.getShipInfo(ref);

        PSMGameObject ship = buildShip(shipInfo);
        captaincyRegister(ship);
        recruitMembers(ship);

        return ship;
    }

    private PSMGameObject buildShip(ShipInfo shipInfo) {
        ShipTemplate shipTemplate = shipTemplateRegister.getShipTemplate(shipInfo.getType());

        PSMGameObject ship = new PSMGameObject("ship_" + System.nanoTime());
        ship.addComponent(new Transform()).setZIndex(2);
        ship.addComponent(new RigidBody(shipTemplate.getHull().getBounds()));
        ship.addComponent(new ModelContainer(new ShipModel(shipInfo), shipTemplate));

        RenderableGroup shapes = ship.addComponent(new RenderableGroup());
        shapes.addShapeRenderer("Hull", new SolidShape(shipTemplate.getHull(), DEFAULT_DECK_COLOR, Settings.DEFAULT_STROKE, DEFAULT_HULL_COLOR));
        Segment[] sails = shipTemplate.getSails();
        for (int i = 0; i < sails.length; i++) {
            Segment sail = sails[i];
            shapes.addShapeRenderer("sail_" + i, new SolidShape(sail, Settings.DEFAULT_STROKE, DEFAULT_HULL_COLOR));
        }
        for (Board board : shipTemplate.getBoards().values()) {
            shapes.addShapeRenderer(board.getSide().name(), new SolidShape(board, Settings.DEFAULT_STROKE, Color.BLACK));
        }

        return ship;
    }

    private void captaincyRegister(PSMGameObject ship) {
        DescriptionProvider provider = new DescriptionProvider();
        provider.createCollector("Nom", go -> go.getComponent(ModelContainer.class).getModel(ShipModel.class).getName());
        provider.createCollector("Nation", go -> go.getComponent(ModelContainer.class).getModel(ShipModel.class).getInfo().getNation().name());
        provider.createCollector("Amiral", go -> go.getComponent(ModelContainer.class).getModel(ShipModel.class).getAdmiral().getName());
        provider.createCollector("Mouvement", go -> go.getComponent(ModelContainer.class).getModel(ShipModel.class).getInfo().getMoveInfos().getInfos());
        provider.createCollector("Cargo", go -> Integer.toString(go.getComponent(ModelContainer.class).getModel(ShipModel.class).getInfo().getCargo()));
        provider.createCollector("Cannons", go -> new String(go.getComponent(ModelContainer.class).getModel(ShipModel.class).getInfo().getCannonInfos().getInfos()));

        ship.addComponent(provider);
    }

    private void recruitMembers(PSMGameObject ship) {
        ship.addComponent(new Pilot());
    }
}
