package mova.psm.sound;

import mova.game.sound.SoundSystem;
import mova.psm.asset.MediaCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class SoundSystemConfig {

    private final Logger logger = LoggerFactory.getLogger(SoundSystemConfig.class);
    private final ApplicationContext applicationContext;

    @Value("${psm.sound-system.enabled:false}")
    private boolean soundSystemEnabled;

    @Autowired
    public SoundSystemConfig(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @PostConstruct
    public void init() {
        if (soundSystemEnabled) {
            logger.debug("Initialisation du sound system");
            mova.game.sound.SoundSystem.start();

            MediaCache mediaCache = applicationContext.getBean(MediaCache.class);
            SoundSystem.setMediaProvider(mediaCache::getResource);
        }
    }
}
