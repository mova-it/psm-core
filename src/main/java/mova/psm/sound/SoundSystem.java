package mova.psm.sound;

import mova.game.event.Event;
import mova.game.event.Observer;
import org.springframework.stereotype.Component;

@Component
public class SoundSystem implements Observer {

    private static final String MAIN_MUSIC_THEME = "hoist-the-colour.mp3";

    @Override
    public void onNotify(Event event) {
        switch (event.getType()) {
            case PSM_START:
            case GAME_END: {
                mova.game.sound.SoundSystem.play(MAIN_MUSIC_THEME);
                break;
            }
            case PSM_STOP:
            case GAME_START: {
                mova.game.sound.SoundSystem.stop(MAIN_MUSIC_THEME);
                break;
            }
        }
    }
}
