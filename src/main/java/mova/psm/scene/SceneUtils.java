package mova.psm.scene;

import mova.game.graphics.ImageUtils;
import mova.game.graphics.MovaGraphics;
import mova.game.graphics.component.GameButton;
import mova.game.graphics.component.GameWindow;
import mova.game.graphics.component.IconSet;

import javax.swing.*;
import java.awt.*;

public final class SceneUtils {

    private SceneUtils() {}

    public static GameButton createIconButton(String tooltip, IconSet iconSet) {
        GameButton button = new GameButton(tooltip);
        button.setIconSet(iconSet, tooltip);

        return button;
    }

    public static IconSet createIconSet(GameWindow gameWindow, ImageIcon iconRollover) {
        int width = iconRollover.getIconWidth();
        int height = iconRollover.getIconHeight();

        Image image = ImageUtils.createCompatibleImage(gameWindow, width, height, Transparency.TRANSLUCENT);
        MovaGraphics g = new MovaGraphics((Graphics2D) image.getGraphics());
        Composite alpha = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, .5f);
        g.setComposite(alpha);
        g.drawImage(iconRollover.getImage());
        g.dispose();
        ImageIcon iconDefault = new ImageIcon(image);

        image = ImageUtils.createCompatibleImage(gameWindow, width, height, Transparency.TRANSLUCENT);
        g = new MovaGraphics((Graphics2D) image.getGraphics());
        g.drawImage(iconRollover.getImage(), 2, 2);
        g.dispose();
        ImageIcon iconPressed = new ImageIcon(image);

        return new IconSet(iconDefault, iconRollover, iconPressed);
    }
}
