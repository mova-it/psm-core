package mova.psm.core.ship;

import mova.game.geom.Locator;
import mova.trigo.TrigoUtils;

import java.awt.geom.AffineTransform;

public final class Compass {

    private static final AffineTransform scratchTransformation = new AffineTransform();

    private final float shipSpeed;
    private boolean tackSpeedCalculated = false;
    private double currentTackSpeed;

    public Compass(float shipSpeed) {
        this.shipSpeed = shipSpeed;
    }

    public AffineTransform nextMovement(long elapsedTime, Locator proue, Locator poupe, Locator destination) {
        scratchTransformation.setToIdentity();

        // On calcule la prochaine translation
        nextTranslation(elapsedTime, proue, destination);
        // S'il y a une translation, on calcule la prochaine rotation
        if (!scratchTransformation.isIdentity()) nextRotation(elapsedTime, proue, poupe, destination);

        return scratchTransformation;
    }

    public AffineTransform toDestination(Locator proue, Locator poupe, Locator destination) {
        scratchTransformation.setToIdentity();

        // On calcule la prochaine translation
        double dx = destination.getX() - proue.getX();
        double dy = destination.getY() - proue.getY();

        if (dx != 0 || dy != 0) {
            scratchTransformation.translate(dx, dy);

            double x1 = -dx;
            double y1 = -dy;
            double x2 = poupe.getX() - proue.getX();
            double y2 = poupe.getY() - proue.getY();

            double teta = TrigoUtils.substractAtan2(y1, x1, y2, x2);
            teta = TrigoUtils.normalize(teta);

            scratchTransformation.rotate(teta, proue.getX(), proue.getY());
        }

        return scratchTransformation;
    }

    public void notifyCourseChange() {
        tackSpeedCalculated = false;
    }

    private void nextTranslation(long elapsedTime, Locator proue, Locator destination) {
        double dx = destination.getX() - proue.getX();
        double dy = destination.getY() - proue.getY();

        if (dx != 0 || dy != 0) {
            double distance = Math.sqrt(dx*dx + dy*dy);

            double remaining = Math.min(shipSpeed*elapsedTime, distance);
            double ratio = remaining/distance;
            dx *= ratio;
            dy *= ratio;
            scratchTransformation.translate(dx, dy);
        }
    }

    private void nextRotation(long elapsedTime, Locator proue, Locator poupe, Locator destination) {
        double dx = scratchTransformation.getTranslateX();
        double dy = scratchTransformation.getTranslateY();
        double x1 = -dx;
        double y1 = -dy;
        double x2 = poupe.getX() - proue.getX();
        double y2 = poupe.getY() - proue.getY();

        double teta = TrigoUtils.substractAtan2(y1, x1, y2, x2);
        teta = TrigoUtils.normalize(teta);

        if (!tackSpeedCalculated) {
            double distance = destination.distance(proue);

            double td = Math.min(distance, poupe.distance(proue))/shipSpeed;
            currentTackSpeed = teta/td;
            tackSpeedCalculated = true;
        }

        double tack = currentTackSpeed*elapsedTime;
        if (Math.abs(tack) > Math.abs(teta)) tack = teta;
        scratchTransformation.rotate(tack, proue.getX(), proue.getY());
    }
}
