package mova.psm.scene;

import mova.game.core.SceneOrchestrator;
import mova.game.dice.Die;
import mova.game.event.EventSystem;
import mova.game.event.EventType;
import mova.game.graphics.ImageUtils;
import mova.game.graphics.MovaGraphics;
import mova.game.graphics.UpdatableGif;
import mova.game.graphics.component.GameButton;
import mova.game.graphics.component.GamePanel;
import mova.game.graphics.component.GameWindow;
import mova.game.graphics.component.UserInterface;
import mova.game.graphics.component.event.GameActionListener;
import mova.psm.asset.FontCache;
import mova.psm.asset.GifCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.awt.font.TextAttribute;
import java.awt.image.BufferedImage;
import java.util.Collections;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public final class HomeScene extends PSMAbstractScene implements GameActionListener {

    private static final GridBagConstraints MENU_LAYOUT_CONSTRAINTS = new GridBagConstraints();
    static {
        MENU_LAYOUT_CONSTRAINTS.gridwidth = GridBagConstraints.REMAINDER;
        MENU_LAYOUT_CONSTRAINTS.anchor = GridBagConstraints.CENTER;
        MENU_LAYOUT_CONSTRAINTS.fill = GridBagConstraints.HORIZONTAL;
        MENU_LAYOUT_CONSTRAINTS.insets = new Insets(10, 1000, 10, 0);
    }

    private final SceneOrchestrator sceneOrchestrator;
    private final GifCache gifCache;
    private final FontCache fontCache;

    private UpdatableGif homeBackground;
    private UpdatableGif pirateFlag;

    @Autowired
    public HomeScene(SceneOrchestrator sceneOrchestrator, GameWindow gameWindow, GifCache gifCache, FontCache fontCache) {
        super(gameWindow);

        this.sceneOrchestrator = sceneOrchestrator;
        this.gifCache = gifCache;
        this.fontCache = fontCache;
    }

    @Override
    public void start() {
        Font font = fontCache.getResource("WreckedShip_custom.ttf", Font.PLAIN, 60);
        Font underlineFont = fontCache.getResource(font, Collections.singletonMap(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON));

        GameButton playButton = UserInterface.createTextualButton("Jouer", font, underlineFont);
        GameButton quitButton = UserInterface.createTextualButton("Quitter", font, underlineFont);

        gameWindow.getUserInterface().add(playButton);
        gameWindow.getUserInterface().add(quitButton);

        GamePanel buttonPanel = new GamePanel(new GridBagLayout());
        buttonPanel.add(playButton, MENU_LAYOUT_CONSTRAINTS);
        buttonPanel.add(quitButton, MENU_LAYOUT_CONSTRAINTS);

        gameWindow.addContents(buttonPanel);

        playButton.addActionListener(this);
        quitButton.addActionListener(this);

        // TODO le cache doit pouvoir être supprimé et dans ce cas on supprime le principe de variable global pour les ressources. Il faut pouvoir tout nettoyer dans release()
        if (Die.D2.roll() == 1) {
            homeBackground = gifCache.getResource("ezgif.com-gif-maker(1).gif");
        } else {
            homeBackground = gifCache.getResource("ezgif.com-gif-maker(3).gif");
        }

        if (Die.D2.roll() == 1) {
            pirateFlag = gifCache.getResource("pirate-flag-x.gif");
        } else {
            pirateFlag = gifCache.getResource("pirate-flag-16.gif");
        }

        super.start();
    }

    @Override
    public void release() {
        gameWindow.getUserInterface().clear();
        gameWindow.clearContent();

        homeBackground = null;
        pirateFlag = null;
    }

    @Override
    public void update(long dt) {
        homeBackground.update(dt);
        pirateFlag.update(dt);

        if (gameWindow.getUserInterface().get("Quitter").isPressed()) {
            EventSystem.notify(EventType.PSM_STOP);
        }
        else if (gameWindow.getUserInterface().get("Jouer").isPressed()) {
            sceneOrchestrator.setScene(ConfigureGameScene.class);
        }
    }

    @Override
    public void draw(MovaGraphics graphics) {
        graphics.drawImage(homeBackground, gameWindow.getSize());

        BufferedImage flag = (BufferedImage) pirateFlag.getCurrentFrame();
        BufferedImage transparentFlagCache = ImageUtils.createTranslucent(gameWindow, flag, .1f);
        graphics.drawImage(transparentFlagCache, gameWindow.getSize());

        gameWindow.paintContent(graphics);
    }

}
