package mova.psm.scene.game2d;

import java.awt.*;
import java.util.List;
import java.util.*;

public class GameContext {

    private final Rectangle map;

    private final List<Shape> islands = new ArrayList<>();

    private final List<String> admiralsNames = new ArrayList<>();

    private final Map<String, List<String>> admiralsShips = new HashMap<>();

    public GameContext(Rectangle map, Collection<Shape> islands, Collection<String> admiralsNames, Map<String, List<String>> admiralsShips) {
        this.map = map;
        this.islands.addAll(islands);
        this.admiralsNames.addAll(admiralsNames);
        this.admiralsShips.putAll(admiralsShips);
    }

    public Rectangle getMap() {
        return map;
    }

    public List<Shape> getIslands() {
        return islands;
    }

    public List<String> getAdmiralsNames() {
        return admiralsNames;
    }

    public Map<String, List<String>> getAdmiralsShips() {
        return admiralsShips;
    }
}
