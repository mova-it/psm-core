package mova.psm.core.ship;

import mova.Settings;
import mova.game.core.ecs.AbstractGameComponent;
import mova.game.core.ecs.GameObject;
import mova.game.dice.Die;
import mova.game.geom.Locator;
import mova.game.geom.Segment;
import mova.game.graphics.*;
import mova.game.model.ModelContainer;
import mova.game.view.Projection;
import mova.lang.MathUtils;
import mova.psm.ecs.PSMGameObject;
import mova.psm.measure.Ruler;
import mova.psm.scene.game2d.GameScene;
import mova.psm.ship.*;
import mova.util.RandomUtils;

import java.awt.*;
import java.util.Arrays;

public class NavalBattery extends AbstractGameComponent {

    private GameObject ship;
    private int currentMastIndex = 0;
    private PSMGameObject[] targets;
    private Segment[] sightLines;

    public void attach(GameObject ship) {
        this.ship = ship;

        getGameObject().getComponent(Transform.class).setTo(ship.getComponent(Transform.class));

        ShipModel shipModel = ship.getComponent(ModelContainer.class).getModel(ShipModel.class);
        targets = new PSMGameObject[shipModel.countMasts()];
        sightLines = new Segment[shipModel.countMasts()];
        currentMastIndex = shipModel.getNearestUpMastIndex(ShipPart.BOW);
    }

    public int getCurrentMastIndex() {
        return currentMastIndex;
    }

    public void moveToNextMast() {
        moveToMast(currentMastIndex + 1);
    }

    public void moveToPreviousMast() {
        moveToMast(currentMastIndex - 1);
    }

    public void moveToMast(int index) {
        currentMastIndex = MathUtils.euclidianModulo(index, ship.getComponent(ModelContainer.class).getModel(ShipModel.class).countMasts());
    }

    public boolean hasTarget() {
        return hasTarget(currentMastIndex);
    }

    private boolean hasTarget(int index) {
        return targets[index] != null;
    }

    public void releaseTarget() {
        releaseTarget(currentMastIndex);
    }

    private void releaseTarget(int index) {
        targets[index] = null;
        sightLines[index] = null;
    }

    @Override
    public void draw(MovaGraphics graphics) {
        Projection sceneProjection = getGameObject().getScene().getCamera().getScreenProjection();

        graphics.setColor(Color.GREEN);
        for (Segment sightLine : sightLines) {
            if (sightLine != null) graphics.draw(sceneProjection.transform(sightLine));
        }
    }

    public boolean aim(Locator mouseLocation) {
        ShipModel shipModel = ship.getComponent(ModelContainer.class).getModel(ShipModel.class);
        Range cannonRange = shipModel.getMast(currentMastIndex).getCannon().getRange();

        ShipTemplate shipTemplate = ship.getComponent(ModelContainer.class).getModel(ShipTemplate.class);
        Locator mastLocation = shipTemplate.getMasts()[currentMastIndex];
        Locator mastWorldLocation = getGameObject().getComponent(Transform.class).apply(mastLocation);

        GameScene scene = (GameScene) getGameObject().getScene();
        Projection screenProjection = scene.getCamera().getScreenProjection();
        Locator worldMouseLocation = screenProjection.inverseTransform(mouseLocation);

        Locator worldLocation = Ruler.capToRange(mastWorldLocation, worldMouseLocation, cannonRange);

        PSMGameObject target = (PSMGameObject) scene.getGameObjects().findGameObject(worldLocation);
        // Si l'objet sur lequel on clique n'est pas null et est le même que celui au bout du Ruler, on valide l'action
        if (target != null && scene.getGameObjects().findGameObject(worldMouseLocation) == target) {
            lockTarget(currentMastIndex, target, new Segment(mastWorldLocation, worldLocation));
            return true;
        }

        return false;
    }

    private void lockTarget(int index, PSMGameObject ship, Segment sightLine) {
        targets[index] = ship;
        sightLines[index] = sightLine;
    }

    public void dismiss() {
        Arrays.fill(targets, null);
        Arrays.fill(sightLines, null);
    }

    public void fire() {
        ShipModel shipModel = ship.getComponent(ModelContainer.class).getModel(ShipModel.class);

        for (int i = 0; i < targets.length; i++) {
            if (hasTarget(i)) {

                PSMGameObject target = targets[i];
                ShipModel targetShipModel = target.getComponent(ModelContainer.class).getModel(ShipModel.class);
                if (!targetShipModel.isSunken() && fire(shipModel.getMast(i).getCannon())) {
                    if (targetShipModel.isAfloat()) {
                        destroyRandomMast(target, targetShipModel);
                    } else {
                        sinkShip(target, targetShipModel);
                    }
                }

                releaseTarget(i);
            }
        }
    }

    private boolean fire(Cannon cannon) {
        int result = Die.D6.roll();
        return !(result == 1 || result <= cannon.getLevel());
    }

    private static void destroyRandomMast(PSMGameObject target, ShipModel targetShipModel) {
        int destroyedMastIndex;
        do destroyedMastIndex = RandomUtils.randInt(targetShipModel.countMasts());
        while (targetShipModel.getMast(destroyedMastIndex).isDestroyed());

        targetShipModel.getMast(destroyedMastIndex).destroy();

        Locator mastLocation = target.getComponent(ModelContainer.class).getModel(ShipTemplate.class).getMast(destroyedMastIndex);
        target.getComponent(RenderableGroup.class).addShapeRenderer("destroyed mast " + destroyedMastIndex, new SolidShape(new Rectangle((int) mastLocation.getX(), (int) mastLocation.getY(), 10, 10), Settings.DEFAULT_STROKE, Color.RED));
    }

    private static void sinkShip(PSMGameObject target, ShipModel targetShipModel) {
        targetShipModel.sink();

        RenderableGroup shapeRendererGroup = target.getComponent(RenderableGroup.class);
        Rectangle bounds = shapeRendererGroup.getShapeRenderer("Hull", ShapeRenderer.class).getShape().getBounds();
        shapeRendererGroup.addShapeRenderer("Destroy cross line 1", new SolidShape(new Segment(bounds.getMinX(), bounds.getMinY(), bounds.getMaxX(), bounds.getMaxY()), Settings.DEFAULT_STROKE, Color.RED));
        shapeRendererGroup.addShapeRenderer("Destroy cross line 2", new SolidShape(new Segment(bounds.getMaxX(), bounds.getMinY(), bounds.getMinX(), bounds.getMaxY()), Settings.DEFAULT_STROKE, Color.RED));
    }

}
