package mova.psm.scene.game3d;

import mova.game.core.ecs.GameObject;
import mova.game.graphics.MovaGraphics;
import mova.game.input.GameActionMapping;

import java.util.List;

interface GameMode {

    default void entry() {}

    GameActionMapping getGameActionMapping();

    void update(long dt, List<GameObject> gameObjects);

    void draw(MovaGraphics graphics, List<GameObject> gameObjects);
}
