package mova.psm.scene;

import mova.Settings;
import mova.game.core.AbstractScene;
import mova.game.event.Event;
import mova.game.event.EventSystem;
import mova.game.event.EventType;
import mova.game.graphics.MovaGraphics;
import mova.game.graphics.Screen;
import mova.game.graphics.UpdatableGif;
import mova.game.graphics.component.GameWindow;
import mova.psm.asset.FontCache;
import mova.psm.asset.GifCache;
import mova.psm.asset.ImageCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.awt.font.TextAttribute;
import java.util.Collections;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PreLoadScene extends AbstractScene {

    private boolean loaded = false;
    private final Thread loadingThread;

    private final Screen screen;
    private final GameWindow gameWindow;
    private final FontCache fontCache;
    private final GifCache gifCache;
    private final ImageCache imageCache;

    private UpdatableGif pirateFlag;

    @Autowired
    public PreLoadScene(Screen screen, GameWindow gameWindow, FontCache fontCache, GifCache gifCache, ImageCache imageCache) {
        this.screen = screen;
        this.gameWindow = gameWindow;
        this.fontCache = fontCache;
        this.gifCache = gifCache;
        this.imageCache = imageCache;

        loadingThread = new Thread(new PreLoading(), "PreLoadingThread");
        loadingThread.setUncaughtExceptionHandler((t, e) -> EventSystem.notify(new Event(EventType.PSM_STOP, e)));
    }

    @Override
    public void start() {
        pirateFlag = gifCache.getResource("pirate-flag-x.gif");

        gameWindow.setSize(600, 300);
        gameWindow.center();
        gameWindow.setVisible(true);
        gameWindow.enableBufferStrategy();
        loadingThread.start();
    }

    @Override
    public void update(long dt) {
        if (loaded) {
            gameWindow.setVisible(false);
            screen.setFullScreenWindow(gameWindow);
            EventSystem.notify(EventType.PSM_START);
        } else {
            pirateFlag.update(dt);
        }
    }

    @Override
    public void draw(MovaGraphics graphics) {
        if (!loaded) {
            graphics.drawImage(pirateFlag, gameWindow.getSize());

            if (lastMsg != null) {
                graphics.setFont(Settings.DIALOG_FONT);
                graphics.setColor(Color.WHITE);
                graphics.drawString(lastMsg, 5, 295);
            }
        }
    }

    private String lastMsg = null;

    private void promptLoading(String msg) {
        lastMsg = msg  != null ? "Loading " + msg : null;
    }

    private final class PreLoading implements Runnable {

        @Override
        public void run() {

            promptLoading("WreckedShip_custom.ttf");
            Font font = fontCache.getResource("WreckedShip_custom.ttf", Font.PLAIN, 60);
            promptLoading("WreckedShip_custom.ttf [UNDERLINED]");
            fontCache.getResource(font, Collections.singletonMap(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON));

            promptLoading("ezgif.com-gif-maker(1).gif");
            gifCache.getResource("ezgif.com-gif-maker(1).gif");
            promptLoading("ezgif.com-gif-maker(3).gif");
            gifCache.getResource("ezgif.com-gif-maker(3).gif");
            promptLoading("pirate-flag-x.gif");
            gifCache.getResource("pirate-flag-x.gif");
            promptLoading("pirate-flag-16.gif");
            gifCache.getResource("pirate-flag-16.gif");

            promptLoading("Aldo.jpg");
            imageCache.getResource("Aldo.jpg");
            promptLoading("Sylvane.jpg");
            imageCache.getResource("Sylvane.jpg");
            promptLoading("seamless-sea-bg.jpg");
            imageCache.getResource("seamless-sea-bg.jpg");
            promptLoading("sand-seamless.jpg");
            imageCache.getResource("sand-seamless.jpg");

            promptLoading(null);

            loaded = true;
        }
    }
}
