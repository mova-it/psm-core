package mova.psm.ecs;

import mova.game.core.Copyable;
import mova.game.core.ecs.*;
import mova.game.core.ecs.container.StatefulGameObjectContainer;
import mova.util.Properties;

public final class PSMGameObject extends AbstractGameObject implements ConfigurableGameObject, StatefulGameObject {

    private final Properties properties = new Properties();

    private State state;

    public PSMGameObject(String name) {
        super(name);
        state = State.IDLE;
    }

    private PSMGameObject(PSMGameObject gameObject) {
        super(gameObject.getName() + "_" + System.nanoTime());
        state = gameObject.getState();

        for (GameComponent component : gameObject.getComponents()) {
            if (component instanceof Copyable) {
                addComponent((GameComponent) ((Copyable<?>) component).copy());
            }
        }
    }

    @Override
    public Properties getProperties() {
        return properties;
    }

    @Override
    public State getState() {
        return state;
    }

    @Override
    public void setState(State state) {
        State oldState = this.state;
        this.state = state;

        if (hasScene()) {
            ((StatefulGameObjectContainer) getScene().getGameObjects()).notifyStateChange(this, oldState);
        }
    }

    @Override
    public PSMGameObject copy() {
        return new PSMGameObject(this);
    }
}
