package mova.psm.asset;

import mova.game.asset.ResourceCache;
import mova.game.graphics.GifDecoder;
import mova.game.graphics.UpdatableGif;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;

@Component
public final class GifCache extends ResourceCache<UpdatableGif> {

    @Override
    protected String getResourceRoot() {
        return "/gifs/";
    }

    @Override
    protected UpdatableGif extract(URL url) throws IOException {
        GifDecoder.GifImage gifImage = GifDecoder.read(url);
        return new UpdatableGif(gifImage);
    }
}
