package mova.psm.scene.game3d;

import mova.game.core.ecs.GameObject;
import mova.game.graphics.MovaGraphics;
import mova.game.graphics3D.MovaPolygonRenderer;
import mova.game.graphics3D.PolygonRenderer;
import mova.game.graphics3D.math3D.Camera;
import mova.game.input.GameActionMapping;

import java.util.List;

abstract class BaseGameMode implements GameMode {


    protected final Camera camera;
    protected final PolygonRenderer polygonRenderer;
    protected GameActionMapping gameActionMapping;

    BaseGameMode(GameActionMapping parentMapping, Camera camera, PolygonRenderer polygonRenderer) {
        gameActionMapping = new GameActionMapping(parentMapping);
        this.camera = camera;
        this.polygonRenderer = polygonRenderer;
    }

    @Override
    public GameActionMapping getGameActionMapping() {
        return gameActionMapping;
    }

    @Override
    public void update(long dt, List<GameObject> gameObjects) {
        for (GameObject gameObject : gameObjects) {
            gameObject.update(dt);
        }
    }

    @Override
    public void draw(MovaGraphics graphics, List<GameObject> gameObjects) {
        polygonRenderer.startFrame(graphics);

        // TODO est-il possible de créer plusieurs polygonRenderer ? un part GameObject? qui dessine uniquement sur une portion de l'image du coup?
        //  Cela permettrait du coup d'injecterr les renderer dans le GameObject

        for (GameObject gameObject : gameObjects) {
            ((MovaPolygonRenderer) polygonRenderer).draw(graphics, gameObject);
        }

        polygonRenderer.endFrame(graphics);
    }
}
