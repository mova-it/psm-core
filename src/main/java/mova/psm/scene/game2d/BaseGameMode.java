package mova.psm.scene.game2d;

import mova.game.geom.Locator;
import mova.game.graphics.component.GameWindow;
import mova.game.graphics.component.UserInterface;
import mova.game.input.GameAction;
import mova.game.input.GameActionMapping;
import mova.game.input.GameInputsListener;
import mova.game.input.MouseUserAction;
import mova.game.view.SimpleCamera;

import java.awt.event.KeyEvent;

abstract class BaseGameMode implements GameMode {

    private static final float ZOOM_FACTOR = .05f;

    protected final GameScene scene;
    protected final GameWindow gameWindow;
    protected final GameInputsListener gameInputsListener;
    protected final GameActionMapping gameActionMapping = new GameActionMapping();


    private final GameAction moveCamRightAction;
    private final GameAction moveCamLeftAction;
    private final GameAction moveCamDownAction;
    private final GameAction moveCamUpAction;
    private final GameAction zoomInAction;
    private final GameAction zoomOutAction;

    BaseGameMode(GameScene scene, GameWindow gameWindow, GameInputsListener gameInputsListener) {
        this.scene = scene;
        this.gameInputsListener = gameInputsListener;
        this.gameWindow = gameWindow;

        moveCamRightAction = gameActionMapping.mapToMouse("Move Cam Right", MouseUserAction.DRAG_LEFT, GameAction.Behavior.NORMAL);
        moveCamLeftAction = gameActionMapping.mapToMouse("Move Cam Left", MouseUserAction.DRAG_RIGHT, GameAction.Behavior.NORMAL);
        moveCamDownAction = gameActionMapping.mapToMouse("Move Cam Down", MouseUserAction.DRAG_UP, GameAction.Behavior.NORMAL);
        moveCamUpAction = gameActionMapping.mapToMouse("Move Cam Up", MouseUserAction.DRAG_DOWN, GameAction.Behavior.NORMAL);
        zoomInAction = gameActionMapping.mapToMouse("Zoom In", MouseUserAction.WHEEL_DOWN, GameAction.Behavior.NORMAL);
        zoomOutAction = gameActionMapping.mapToMouse("Zoom Out", MouseUserAction.WHEEL_UP, GameAction.Behavior.NORMAL);

        UserInterface ui = scene.getUserInterface();
        GameAction quitAction = ui.get("Quit").getGameAction();
        gameActionMapping.mapToKey(quitAction, KeyEvent.VK_ESCAPE);
    }

    @Override
    public void entry() {
        gameActionMapping.resetAllGameActions();
        gameInputsListener.setGameActionMapping(gameActionMapping);
    }


    @Override
    public void update(long dt) {
        Locator mouseLocation = gameInputsListener.getMouseLocation();

        if (!gameInputsListener.getMouseState(MouseUserAction.LEFT_BUTTON)) {
            int horizontalMove = computeHorizontalMove(mouseLocation, dt);
            horizontalMoveAction(horizontalMove);

            int verticalMove = computeVerticalMove(mouseLocation, dt);
            verticalMoveAction(verticalMove);
        } else {
            int horizontalMove = moveCamRightAction.getAmount() - moveCamLeftAction.getAmount();
            horizontalMoveAction(horizontalMove);

            int verticalMove = moveCamUpAction.getAmount() - moveCamDownAction.getAmount();
            verticalMoveAction(verticalMove);
        }

        // Gestion du zoom
        int zoom = zoomOutAction.getAmount() - zoomInAction.getAmount();
        zoomAction(zoom, mouseLocation);
    }

    @Override
    public void release() {
        gameActionMapping.resetAllGameActions();
    }

    private int computeHorizontalMove(Locator mouseLocation, long dt) {
        return computeMove(mouseLocation.getX(), 0, gameWindow.getWidth() - 1, dt);
    }

    private int computeVerticalMove(Locator mouseLocation, long dt) {
        return computeMove(mouseLocation.getY(), gameWindow.getHeight() - 1, 0, dt);
    }

    private int computeMove(double mouseValue, int negMoveBound, int posMoveBound, long dt) {
        if (mouseValue == posMoveBound) return (int) scene.getCamera().getScreenProjection().unscaleDistance(dt);
        else if (mouseValue == negMoveBound) return (int) -scene.getCamera().getScreenProjection().unscaleDistance(dt);
        else return 0;
    }

    private void horizontalMoveAction(float horizontalMovement) {
        scene.getCamera().addX(horizontalMovement);
    }

    private void verticalMoveAction(float verticalMovement) {
        scene.getCamera().addY(verticalMovement);
    }

    private void zoomAction(float zoom, Locator mouseLocation) {
        if (zoom != 0) {
            ((SimpleCamera) scene.getCamera()).increaseScaleFactor(zoom * ZOOM_FACTOR, mouseLocation);
        }
    }
}
