package mova.psm.graphics;

import mova.game.graphics.NullRepaintManager;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class UiConfig {

    @PostConstruct
    private void init() {
        LoggerFactory.getLogger(UiConfig.class).debug("Configuration de l'interface utilisateur");
    }

    static {
        NullRepaintManager.install();
    }
}
