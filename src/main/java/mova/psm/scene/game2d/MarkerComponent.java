package mova.psm.scene.game2d;

import mova.game.core.ecs.AbstractGameComponent;
import mova.game.graphics.SolidShape;
import mova.game.graphics.Transform;
import mova.game.physics.RigidBody;
import mova.psm.ecs.PSMGameObject;

import java.awt.*;
import java.awt.geom.Ellipse2D;

final class MarkerComponent extends AbstractGameComponent {

    public void moveTo(PSMGameObject selectedGameObject) {
        if (selectedGameObject != null) {
            PSMGameObject marker = (PSMGameObject) getGameObject();
            RigidBody body = selectedGameObject.getComponent(RigidBody.class);
            if (body != null) {
                Rectangle hullBounds = body.getBodyShape().getBounds();
                double diameter = hullBounds.getWidth();

                SolidShape solidShape = marker.getComponent(SolidShape.class);
                Ellipse2D circle = (Ellipse2D) solidShape.getShape();
                circle.setFrame(0, 0, diameter, diameter);

                Transform markerTransformation = marker.getComponent(Transform.class);
                markerTransformation.setToTranslation(-diameter / 2, -diameter / 2);
                markerTransformation.addTranslation(hullBounds.getCenterX(), hullBounds.getCenterY());

                Transform transform = selectedGameObject.getComponent(Transform.class);
                if (transform != null) markerTransformation.add(transform);
            }
        }
    }
}



