package mova.psm.scene.game2d;

import mova.Settings;
import mova.game.event.EventSystem;
import mova.game.event.EventType;
import mova.game.graphics.component.GameWindow;
import mova.game.input.GameInputsListener;
import mova.psm.ecs.PSMGameObject;

public final class WinMode extends BaseGameMode {

    private PSMGameObject winnerObject;
    private int screenTime = 0;

    WinMode(GameScene scene, GameWindow gameWindow, GameInputsListener gameInputsListener) {
        super(scene, gameWindow, gameInputsListener);
    }

    @Override
    public void entry() {
        gameActionMapping.resetAllGameActions();
        gameInputsListener.setGameActionMapping(gameActionMapping);

        SplashScreen splashScreen = new SplashScreen();
        splashScreen.setSize(gameWindow.getSize());
        splashScreen.setFont(Settings.DIALOG_FONT.deriveFont(50f));
        splashScreen.setText(scene.getCurrentAdmiral().getName() + " gagne la partie!!!");

        winnerObject = new PSMGameObject("THE WINNER IS");
        winnerObject.addComponent(splashScreen);
        scene.addGameObject(winnerObject);
        winnerObject.show();
    }

    @Override
    public void update(long dt) {
        screenTime += dt;
    }

    @Override
    public Class<? extends GameMode> handleModeChange() {
        if (screenTime >= 5000) {
            winnerObject.destroy();
            EventSystem.notify(EventType.GAME_END);
        }

        return null;
    }
}
