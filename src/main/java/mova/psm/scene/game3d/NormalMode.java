package mova.psm.scene.game3d;

import mova.game.core.ecs.GameObject;
import mova.game.graphics3D.PolygonRenderer;
import mova.game.graphics3D.math3D.Camera;
import mova.game.graphics3D.math3D.Rotation3D;
import mova.game.graphics3D.math3D.Transform3D;
import mova.game.input.GameAction;
import mova.game.input.GameActionMapping;
import mova.game.input.MouseUserAction;
import mova.trigo.Angle;

import java.awt.event.KeyEvent;
import java.util.List;

class NormalMode extends BaseGameMode {

    private final GameAction goForward;
    private final GameAction goBackward;
    private final GameAction goLeft;
    private final GameAction goRight;
    private final GameAction goUp;
    private final GameAction goDown;
    private final GameAction turnLeft;
    private final GameAction turnRight;
    private final GameAction tiltUp;
    private final GameAction tiltDown;

    NormalMode(GameActionMapping parentMapping, Camera camera, PolygonRenderer polygonRenderer) {
        super(parentMapping, camera, polygonRenderer);

        goForward = gameActionMapping.mapToKey("Forward", KeyEvent.VK_Z, GameAction.Behavior.NORMAL);
        goBackward = gameActionMapping.mapToKey("Backward", KeyEvent.VK_S, GameAction.Behavior.NORMAL);
        goLeft = gameActionMapping.mapToKey("Left", KeyEvent.VK_Q, GameAction.Behavior.NORMAL);
        goRight = gameActionMapping.mapToKey("Right", KeyEvent.VK_D, GameAction.Behavior.NORMAL);
        goUp = gameActionMapping.mapToKey("Up", KeyEvent.VK_PAGE_UP, GameAction.Behavior.NORMAL);
        goDown = gameActionMapping.mapToKey("Down", KeyEvent.VK_PAGE_DOWN, GameAction.Behavior.NORMAL);

        turnLeft = gameActionMapping.mapToMouse("Left", MouseUserAction.DRAG_LEFT, GameAction.Behavior.NORMAL);
        turnRight = gameActionMapping.mapToMouse("Right", MouseUserAction.DRAG_RIGHT, GameAction.Behavior.NORMAL);
        tiltUp = gameActionMapping.mapToMouse("Tilt Up", MouseUserAction.DRAG_UP, GameAction.Behavior.NORMAL);
        tiltDown = gameActionMapping.mapToMouse("Tilt Down", MouseUserAction.DRAG_DOWN, GameAction.Behavior.NORMAL);
    }

    @Override
    public void update(long dt, List<GameObject> gameObjects) {
        float distanceChange = .5f * dt;

        Transform3D position = camera.getPosition();
        Rotation3D rotation = position.getRotation();

        // apply movement
        Angle angleY = rotation.getAngleY();
        if (goForward.isPressed()) {
            camera.subWorldX(distanceChange * angleY.sin());
            camera.subWorldZ(distanceChange * angleY.cos());
        }
        if (goBackward.isPressed()) {
            camera.addWorldX(distanceChange * angleY.sin());
            camera.addWorldZ(distanceChange * angleY.cos());
        }
        if (goLeft.isPressed()) {
            camera.subWorldX(distanceChange * angleY.cos());
            camera.addWorldZ(distanceChange * angleY.sin());
        }
        if (goRight.isPressed()) {
            camera.addWorldX(distanceChange * angleY.cos());
            camera.subWorldZ(distanceChange * angleY.sin());
        }
        if (goUp.isPressed()) {
            camera.addWorldY(distanceChange);
        }
        if (goDown.isPressed()) {
            camera.subWorldY(distanceChange);
        }


        float angleChange = 0.0002f * dt;

        // look up/down (rotate around x)
        int tilt = tiltUp.getAmount() - tiltDown.getAmount();
        tilt = Math.min(tilt, 200);
        tilt = Math.max(tilt, -200);

        // limit how far you can look up/down
        Angle newAngle = new Angle(tilt*angleChange);
        newAngle.add(rotation.getAngleX());
        newAngle.capCos(0, 1);
        rotation.getAngleX().set(newAngle);

        // turn (rotate around y)
        int turn = turnLeft.getAmount() - turnRight.getAmount();
        rotation.addAngleY(turn * angleChange);

        super.update(dt, gameObjects);
    }
}
