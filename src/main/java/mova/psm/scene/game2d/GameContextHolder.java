package mova.psm.scene.game2d;

public class GameContextHolder {

    private static GameContext currentGameContext;

    private GameContextHolder() {}

    public static void setGameContext(GameContext gameContext) {
        currentGameContext = gameContext;
    }

    public static GameContext getGameContext() {
        if (currentGameContext == null) throw new IllegalStateException("La partie n'a pas été initialisée");

        return currentGameContext;
    }
}
