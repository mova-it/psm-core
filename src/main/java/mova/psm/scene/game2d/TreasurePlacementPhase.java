package mova.psm.scene.game2d;

import mova.game.model.ModelContainer;
import mova.psm.treasure.Doublon;
import mova.psm.map.IslandModel;
import mova.psm.treasure.Treasure;
import mova.util.RandomUtils;

import java.util.concurrent.atomic.AtomicInteger;

class TreasurePlacementPhase extends GameModePhase<ConfigurationMode> {

    public static final int DEFAULT_TREASURES_NUMBER_PER_ISLAND = 4;
    public static final int DEFAULT_GOLD_COINS_NUMBER_PER_PLAYER = 15;

    protected TreasurePlacementPhase(ConfigurationMode mode) {
        super(mode);
    }

    @Override
    void entry() {

        // Sur une base de 6 iles donc 2 base islands => 4 iles sauvages, 16 piècces => 4 pièces par île
        /*Each treasure coin is printed with a number indicating how much gold it is worth. For a 40-point
game, each player should contribute 8 treasure coins totaling 15 gold points. Shuffle the treasure
with the numbers face down, and then randomly distribute four coins to each wild island.*/

        int nbAdmirals = mode.scene.admirals.size();
        int nbWildIslands = GameContextHolder.getGameContext().getIslands().size() - nbAdmirals;
        int nbTreasures = nbWildIslands * DEFAULT_TREASURES_NUMBER_PER_ISLAND;
        int nbGoldPoints = nbAdmirals * DEFAULT_GOLD_COINS_NUMBER_PER_PLAYER;

        Treasure[] treasures = RandomUtils.shuffle(Doublon.createTreasures(nbTreasures, nbGoldPoints));
        AtomicInteger islandCounter = new AtomicInteger();

        // TODO on a toujours un problème d'affichage pour les trop longue phrase dans la description de l'île par exemple pour le trésor
        mode.scene.getGameObjects().forEach(gameObject -> {
            ModelContainer modelContainer = gameObject.getComponent(ModelContainer.class);
            if (modelContainer != null) {
                IslandModel model = modelContainer.getModel(IslandModel.class);
                if (model != null && model.isWild()) {
                    for (int i = 0; i < DEFAULT_TREASURES_NUMBER_PER_ISLAND; i++) {
                        model.addTreasure(treasures[i + DEFAULT_TREASURES_NUMBER_PER_ISLAND*islandCounter.get()]);
                    }

                    islandCounter.incrementAndGet();
                }
            }
        });

        mode.nextPhase();
    }

}
