package mova.psm.core;

import mova.benchmark.MemMonitor;
import mova.debug.DebugUtils;
import mova.game.core.GameCore;
import mova.game.core.GameLoopType;
import mova.game.core.Scene;
import mova.game.core.SceneOrchestrator;
import mova.game.core.ecs.GameObject;
import mova.game.event.Event;
import mova.game.event.EventType;
import mova.game.event.Observer;
import mova.game.graphics.MovaGraphics;
import mova.game.graphics.Screen;
import mova.game.graphics.component.GameWindow;
import mova.game.time.FPSCalculator;
import mova.lang.ThreadUtils;
import mova.psm.debug.ErrorReporter;
import mova.psm.scene.HomeScene;
import mova.psm.scene.PreLoadScene;
import mova.util.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.awt.*;

@Component
public final class PSMGameCore extends GameCore implements Observer {

    private final Screen screen;
    private final GameWindow gameWindow;
    private final SceneOrchestrator sceneOrchestrator;
    private final ErrorReporter errorReporter;
    private MemMonitor monitor;

    @Autowired
    public PSMGameCore(Screen screen, GameWindow gameWindow, SceneOrchestrator sceneOrchestrator, ErrorReporter errorReporter) {
        super(GameLoopType.BASIC);

        this.screen = screen;
        this.gameWindow = gameWindow;
        this.sceneOrchestrator = sceneOrchestrator;
        this.errorReporter = errorReporter;
    }

    @Override
    protected void init() {
        monitor = new MemMonitor();

        sceneOrchestrator.setScene(PreLoadScene.class);
    }

    @Override
    protected MovaGraphics getGraphics() {
        return gameWindow.getGraphics(true);
    }

    @Override
    public void update(long dt) {
        sceneOrchestrator.getCurrentScene().update(dt);
    }

    @Override
    public void draw(MovaGraphics graphics, long dt) {
        sceneOrchestrator.getCurrentScene().draw(graphics);

        DebugUtils.drawDebugLine(graphics, String.format("%.2f frames/sec; %d ms deltaTime", FPSCalculator.tic(dt), dt), Color.WHITE);
    }

    @Override
    protected void atLoopEnd() {
        gameWindow.updateBufferStrategy();

        monitor.takeSample();
        DebugUtils.resetDebugSession();
    }

    @Override
    protected void beforeExit() {
        try {
            screen.dispose();
        } finally {
            ThreadUtils.lazilyExit();

            if (logger.isDebugEnabled()) {
                logger.debug("Rapport de monitoring:\n{}", monitor.getReport());
            }
        }
    }

    @Override
    protected void onUncaughtError(Throwable t) {
        Scene scene = sceneOrchestrator.getCurrentScene();
        GameObject[] gameObjects = scene.getGameObjects().getGameObjectsView();

        errorReporter.report(t, gameObjects);
    }

    @Override
    public void stop() {
        super.stop();

        if (logger.isDebugEnabled()) {
            Scene scene = sceneOrchestrator.getCurrentScene();
            GameObject[] gameObjects = scene.getGameObjects().getGameObjectsView();

            if (!ArrayUtils.isEmpty(gameObjects)) {
                StringBuilder builder = new StringBuilder("Liste des GameObjects non-détruits:");
                for (GameObject gameObject : gameObjects) {
                    builder.append("\t- ").append(gameObject.getName());
                }
                logger.debug(builder.toString());
            } else {
                logger.debug("Tous les GameObjects ont bien été nettoyés");
            }
        }
    }

    @Override
    public void onNotify(Event event) {
        if (event.getType() == EventType.PSM_STOP) stop();
        else if (event.getType() == EventType.PSM_START) sceneOrchestrator.setScene(HomeScene.class);
        else if (event.getType() == EventType.GAME_END) sceneOrchestrator.setScene(HomeScene.class);
    }
}
