package mova.psm.scene.game2d;

import mova.game.core.Scene;
import mova.game.core.ecs.AbstractGameComponent;
import mova.game.core.ecs.GameObject;
import mova.game.geom.Locator;
import mova.game.graphics.MovaGraphics;
import mova.game.graphics.SolidShape;
import mova.game.graphics.Transform;
import mova.game.input.GameInputsListener;
import mova.game.model.ModelContainer;
import mova.game.view.Projection;
import mova.psm.core.ship.Pilot;
import mova.psm.ecs.PSMGameObject;
import mova.psm.measure.AngleConstraints;
import mova.psm.measure.Route;
import mova.psm.measure.Ruler;
import mova.psm.measure.RulerDimensioner;
import mova.psm.ship.MovementPool;
import mova.psm.ship.ShipModel;

import java.awt.*;

final class GhostSimulator extends AbstractGameComponent {

    private final GameInputsListener gameInputsListener;
    private final PSMGameObject ship;
    private final Route route;
    private AngleConstraints maneuverRange = null;

    private GameObject[] gameObjects;

    public GhostSimulator(PSMGameObject ship, GameInputsListener gameInputsListener) {
        this.gameInputsListener = gameInputsListener;
        this.ship = ship;

        Transform currentTransformation = ship.getComponent(Transform.class);
        Pilot pilot = ship.getComponent(Pilot.class);

        Locator proue = pilot.getCurrentBoard().getProue();
        proue = currentTransformation.apply(proue);
        this.route = new Route(proue);
    }

    @Override
    public void start() {
        gameObjects = getGameObject().getScene().getGameObjects().getGameObjectsView();
    }

    public void setManeuverRange(AngleConstraints maneuverRange) {
        this.maneuverRange = maneuverRange;
    }

    public Route getRoute() {
        return route;
    }

    public void simulate(Locator mouseLocation, AngleConstraints angleConstraints) {

        GameObject ghost = getGameObject();
        Scene scene = ghost.getScene();

        // On reset le marqueur en blanc
        SolidShape markerCircle = scene.getGameObjects().findGameObject(GameScene.MARKER_GAME_OBJECT_NAME).getComponent(SolidShape.class);
        markerCircle.setDrawColor(Color.WHITE);

        // On reset la potentiel ancienne collision du bateau courant
//        Pilot pilot = ship.getComponent(Pilot.class);
//        pilot.setColliding(false);

        // On reset la potentiel ancienne collision du bateau fantome
        Pilot ghostPilot = ghost.getComponent(Pilot.class);
//        ghostPilot.setColliding(false);

        // On récupère le pool de mouvements et s'il en reste
        ShipModel currentShipModel = ship.getComponent(ModelContainer.class).getModel(ShipModel.class);
        MovementPool movementPool = currentShipModel.getMovementPool();
        if (movementPool.hasMovement()) {

            // On reset le ghost à la place du bateau courant
            Transform transform = ship.getComponent(Transform.class);
            Transform ghostTransform = ghost.getComponent(Transform.class);
            ghostTransform.setTo(transform);

            // On copie la route du bateau courant
            Route ghostRoute = route.copy();

            // On projète le point de la souris dans le monde
            Projection screenProjection = scene.getCamera().getScreenProjection();
            Locator worldLocation = screenProjection.inverseTransform(mouseLocation);
            worldLocation = angleConstraints.apply(ghostRoute.getCurrentCourse(), worldLocation);

            // On ajoute le point courant comme cap suivant dans la route
            ghostRoute.addCourse(Ruler.capToRange(ghostRoute.getCurrentCourse(), worldLocation, movementPool.greaterMovement()));

            // On donne la route au pilot fantôme
            ghostPilot.followRoute(ghostRoute);

            ghostPilot.toRouteEnd();

            // TODO la collision consiste en fait à s'assurer que le bateau arrive sur un espace libre et que la LIGNE de déplacement ne traverse pas d'obstacle!!!
            // TODO on peut s'inspirer du Physics2D du projet mario: Un "ColiderManager" qu'on initialise avec tous les gameObjects concerné
            // TODO lorsqu'il y a collision, la rotation s'arrête précocément. Le bateau ne sera donc pas aligné avec le cap alors qu'il le devrait
//            RigidBody ghostBody = ghost.getComponent(RigidBody.class);


            // S'il y a une collision le bateau ne va pas au bout de la règle, on force donc la taille de la règle à l'emplacement du bateau
            Locator ghostProue = ghostTransform.apply(ghostPilot.getCurrentBoard().getProue());
            RulerDimensioner dimensioner = scene.getGameObjects().findGameObject(GameScene.RULER_GAME_OBJECT_NAME).getComponent(RulerDimensioner.class);
            dimensioner.setTo(ghostProue);
        }
    }

    @Override
    public void update(long dt) {
        simulate(gameInputsListener.getMouseLocation(), maneuverRange);
    }

    @Override
    public void draw(MovaGraphics graphics) {
        if (!route.isEmpty()) {
            graphics.setColor(Color.GREEN);

            Projection sceneProjection = getGameObject().getScene().getCamera().getScreenProjection();
            Locator from = sceneProjection.transform(route.getOrigin());
            for (Locator course : route.getCourses()) {
                course = sceneProjection.transform(course);

                graphics.drawLine(from, course);

                from = course;
            }
        }
    }
}
