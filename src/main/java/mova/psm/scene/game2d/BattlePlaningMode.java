package mova.psm.scene.game2d;

import mova.game.geom.Locator;
import mova.game.graphics.Transform;
import mova.game.graphics.component.GameWindow;
import mova.game.input.GameAction;
import mova.game.input.GameInputsListener;
import mova.game.input.MouseUserAction;
import mova.game.model.ModelContainer;
import mova.psm.core.ship.NavalBattery;
import mova.psm.ecs.PSMGameObject;
import mova.psm.measure.RulerDimensioner;
import mova.psm.ship.ShipModel;
import mova.psm.ship.ShipTemplate;

import java.awt.event.KeyEvent;

final class BattlePlaningMode extends BaseGameMode {

    private final GameAction prevMastAction;
    private final GameAction nextMastAction;
    private final GameAction lockTargetAction;
    private final GameAction unlockTargetAction;
    private final GameAction validateBattleAction;
    private final GameAction cancelModeAction;

    private PSMGameObject currentShip;

    BattlePlaningMode(GameScene scene, GameWindow gameWindow, GameInputsListener gameInputsListener) {
        super(scene, gameWindow, gameInputsListener);

        prevMastAction = gameActionMapping.mapToKey("Previous Mast", KeyEvent.VK_LEFT);
        nextMastAction = gameActionMapping.mapToKey("Next Mast", KeyEvent.VK_RIGHT);
        lockTargetAction = gameActionMapping.mapToMouse("Lock target", MouseUserAction.LEFT_BUTTON);
        unlockTargetAction = gameActionMapping.mapToKey("Cancel course", KeyEvent.VK_BACK_SPACE);
        gameActionMapping.mapToMouse(unlockTargetAction, MouseUserAction.REAR_BUTTON);
        validateBattleAction = gameActionMapping.mapToKey("Validate battle", KeyEvent.VK_SPACE);
        cancelModeAction = gameActionMapping.mapToKey("Cancel mode", KeyEvent.VK_B);
    }

    @Override
    public void entry() {
        super.entry();

        currentShip = scene.getSelectedGameObject();

        RulerDimensioner dimensioner = scene.getGameObjects().findGameObject(GameScene.RULER_GAME_OBJECT_NAME).getComponent(RulerDimensioner.class);
        dimensioner.setManeuverRange(null);
        dimensioner.autoUpdate(true);

        PSMGameObject cannoneer = (PSMGameObject) scene.getGameObjects().findGameObject(GameScene.CANNONEER_GAME_OBJECT_NAME);
        NavalBattery navalBattery = cannoneer.getComponent(NavalBattery.class);
        navalBattery.attach(currentShip);
        cannoneer.show();

        updateRuler();
    }

    @Override
    public void update(long dt) {
        super.update(dt);

        NavalBattery navalBattery = scene.getGameObjects().findGameObject(GameScene.CANNONEER_GAME_OBJECT_NAME).getComponent(NavalBattery.class);
        if (nextMastAction.isPressed()) {
            navalBattery.moveToNextMast();
            updateRuler();
        }
        if (prevMastAction.isPressed()) {
            navalBattery.moveToPreviousMast();
            updateRuler();
        }

        if (lockTargetAction.isPressed() && !navalBattery.hasTarget()) {
            Locator mouseLocation = gameInputsListener.getMouseLocation();
            boolean locked = navalBattery.aim(mouseLocation);
            if (locked) ((PSMGameObject) scene.getGameObjects().findGameObject(GameScene.RULER_GAME_OBJECT_NAME)).disable();
        }
        if (unlockTargetAction.isPressed() && navalBattery.hasTarget()) {
            navalBattery.releaseTarget();
            ((PSMGameObject) scene.getGameObjects().findGameObject(GameScene.RULER_GAME_OBJECT_NAME)).activate();
        }
    }

    @Override
    public Class<? extends GameMode> handleModeChange() {
        if (validateBattleAction.isPressed()) {
            ((PSMGameObject) scene.getGameObjects().findGameObject(GameScene.RULER_GAME_OBJECT_NAME)).disable();
            return BattlingMode.class;
        }
        if (cancelModeAction.isPressed()) {
            // TODO peut être "caché" le cannoneer
            scene.getGameObjects().findGameObject(GameScene.CANNONEER_GAME_OBJECT_NAME).getComponent(NavalBattery.class).dismiss();
            ((PSMGameObject) scene.getGameObjects().findGameObject(GameScene.RULER_GAME_OBJECT_NAME)).disable();
            return NormalMode.class;
        }

        return null;
    }

    private void updateRuler() {
        NavalBattery navalBattery = scene.getGameObjects().findGameObject(GameScene.CANNONEER_GAME_OBJECT_NAME).getComponent(NavalBattery.class);

        PSMGameObject ruler = (PSMGameObject) scene.getGameObjects().findGameObject(GameScene.RULER_GAME_OBJECT_NAME);
        if (!navalBattery.hasTarget()) {
            int mastIndex = navalBattery.getCurrentMastIndex();

            ShipModel shipModel = currentShip.getComponent(ModelContainer.class).getModel(ShipModel.class);
            RulerDimensioner dimensioner = ruler.getComponent(RulerDimensioner.class);
            dimensioner.setRange(shipModel.getMast(mastIndex).getCannon().getRange());

            ShipTemplate shipTemplate = currentShip.getComponent(ModelContainer.class).getModel(ShipTemplate.class);
            Locator mastLocation = shipTemplate.getMasts()[mastIndex];
            mastLocation = currentShip.getComponent(Transform.class).apply(mastLocation);
            ruler.getComponent(Transform.class).setToLocation(mastLocation);

            ruler.activate();
        } else {
            ruler.disable();
        }
    }
}
